<header class="clearfix">
	<!-- Static navbar -->
	<div class="navbar navbar-default navbar-fixed-top">
		<div class="top-line">
			<div class="container">
				<p>
					<span><i class="fa fa-phone"></i>Agencia: (443) 334 4440</span>
				</p>
				<ul class="social-icons">
					<li><a class="facebook" href="https://www.facebook.com/famemanantiales" target="_blank"><i class="fa fa-facebook"></i></a></li>
					<li><a class="twitter" href="https://twitter.com/manantialesf" target="_blank"><i class="fa fa-twitter"></i></a></li>
					<li><a class="youtube" href="https://www.youtube.com/user/GrupoFameAutos" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
					<li>
						<a class="whatsapp" href="https://api.whatsapp.com/send?phone=524433250949&text=Hola,%20Quiero%20más%20información!" target="_blank">
							<i class="fa fa-whatsapp"></i>
						</a>
					</li>
					<li><a class="instagram" href="https://www.instagram.com/grupofame" target="_blank"><i class="fa fa-instagram"></i></a></li>
					
				</ul>
			</div>
		</div>
		
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.php" target="_self"><img alt="Inicio" src="images/logo.png"></a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right">

                <li><a class="active" href="index.php">Inicio</a></li>
                <li class="drop"><a href="autos.php">Vehículos</a>
				<ul class="drop-down">
                <li><a href="pdfs/enclave2019.pdf" target="_blank">Buick Enclave</a></li>
				<li><a href="pdfs/encore2019.pdf" target="_blank">Buick Encore</a></li>
				<li><a href="pdfs/envision2019.pdf" target="_blank">Buick Envision</a></li>
                <li><a href="pdfs/ats2019.pdf" target="_blank">Cadillac ATS</a></li>	
				<li><a href="pdfs/cts2019.pdf" target="_blank">Cadillac CTS</a></li>
				<li><a href="pdfs/escalade2019.pdf" target="_blank">Cadillac Escalade</a></li>
				<li><a href="pdfs/xt52019.pdf" target="_blank">Cadillac XT5</a></li>
				<li><a href="pdfs/acadia2019.pdf" target="_blank">GMC Acadia</a></li>	
				<li><a href="pdfs/sierradenali2018.pdf" target="_blank">GMC Sierra Denali</a></li>
                <li><a href="pdfs/sierraallterrain2018.pdf" target="_blank">GMC Sierra Crew Cab</a></li>
				<li><a href="pdfs/sierra2018.pdf" target="_blank">GMC Sierra</a></li>	
				<li><a href="pdfs/terrain2019.pdf" target="_blank">GMC Terrain Denali</a></li>
                <li><a href="pdfs/yukondenali2019.pdf" target="_blank">GMC Yukon</a></li>
                             </ul> </li>    

			<li class="drop"><a>Servicio</a>
				<ul class="drop-down">
					<li><a href="servicio.php">Cita de Servicio Morelia</a></li>
					<li><a href="serviciouru.php">Cita de Servicio Uruapan</a></li>
					<li><a href="servicio-buick.php" target="_self">Servicio Buick</a></li>
					<li><a href="servicio-gmc.php" target="_self">Servicio GMC</a></li>
					<li><a href="servicio-cadillac.php" target="_self">Servicio Cadillac</a></li>
					<li><a href="refacciones.php">Refacciones Morelia</a></li>
					<li><a href="refaccionesuru.php">Refacciones Uruapan</a></li>
				</ul> </li> 
							
				<li class="drop"><a href="#">Beneficios</a>
					<ul class="drop-down">
						<li><a href="garantia-buick.php">Garantía Buick</a></li>
						<li><a href="garantia-gmc.php" target="_self">Garantía GMC</a></li>
						<li><a href="garantia-cadillac.php" target="_self">Garantía Cadillac</a></li>
						<li><a href="onstar.php">On Star®</a></li>
						<li><a href="cadillac-shield.php">Cadillac Shield®</a></li>
					</ul> </li>                                   
								
								
								<li class="drop"><a href="promociones.php">Promociones</a>
									<ul class="drop-down">
										<li><a href="promos-buick.php">Buick®</a></li>
										<li><a href="promos-gmc.php">GMC®</a></li>
										<li><a href="promos-cadillac.php">Cadillac®</a></li>
										<li><a href="gm-financial.php">GM® Financial</a></li>
									</ul></li>   
									
									
									<li class="drop"><a>Contacto</a>
										<ul class="drop-down">
											<li><a href="contacto.php">Morelia</a></li>
											<li><a href="contacto-upn.php">Uruapan</a></li>
											
										</ul></li>
										<li><a href="ubicacion.php">Ubicación</a></li>
									</ul>
									
								</div>
							</div>
						</div>

						 
					</header>