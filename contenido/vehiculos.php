<!-- CONTENEDOR COCHES -->
		<div id="content">
			<!-- Modelos 2016 -->
			<div class="latest-post">
				<div class="title-section">
					<h1>Nuestros <span> Vehículos</span></h1>
					<p>Navega a través de la sección para seleccionar el auto de tu preferencia</p>
				</div>
                
				<div id="owl-demo" class="owl-carousel owl-theme">
         			
	               

 					 <div class="item news-item">
	                 <center>
						 <a href="pdfs/yukondenali2019.pdf" target="_blank"><img alt="Yukon Denali 2018" src="images/autos/yukondenali2019.jpg"></a>
						 <h1> <strong> <em> <font color="#000000" SIZE=6 > GMC  </font>  </em> </strong></h1> </font> 
						 <h2> <strong> <em> <font color="#000000"> YUKON DENALI <sup>&reg;</sup> 2019</font> </em> </strong></h2> </font>  
						 <a class="read-more" href="pdfs/yukondenali2019.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                  </center>
 				 </div>

 				 <div class="item news-item">
	                 <center>
						 <a href="pdfs/acadia2019.pdf" target="_blank"><img alt="Acadia 2018" src="images/autos/acadia2019.jpg"></a>
						 <h1> <strong> <em> <font color="#000000" SIZE=6 > GMC  </font>  </em> </strong></h1> </font> 
						 <h2> <strong> <em> <font color="#000000"> ACADIA <sup>&reg;</sup> 2019</font> </em> </strong></h2> </font>  
						 <a class="read-more" href="pdfs/acadia2019.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                  </center>
 				 </div>

 				 <div class="item news-item">
	                 <center>
						 <a href="pdfs/terrain2019.pdf" target="_blank"><img alt="Terrain 2018" src="images/autos/terrain2019.jpg"></a>
						 <h1> <strong> <em> <font color="#000000" SIZE=6 > GMC  </font>  </em> </strong></h1> </font> 
						 <h2> <strong> <em> <font color="#000000"> TERRAIN <sup>&reg;</sup> 2019</font> </em> </strong></h2> </font>  
						 <a class="read-more" href="pdfs/terrain2019.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                  </center>
 				 </div>

 				 <div class="item news-item">
	                 <center>
						 <a href="pdfs/sierra2018.pdf" target="_blank"><img alt="Sierra 2018" src="images/autos/sierra2018.jpg"></a>
						 <h1> <strong> <em> <font color="#000000" SIZE=6 > GMC  </font>  </em> </strong></h1> </font> 
						 <h2> <strong> <em> <font color="#000000"> SIERRA <sup>&reg;</sup> 2019</font> </em> </strong></h2> </font>  
						 <a class="read-more" href="pdfs/sierra2018.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                  </center>
 				 </div>

 				 <div class="item news-item">
	                 <center>
						 <a href="pdfs/sierra2018.pdf" target="_blank"><img alt="Sierra All Terrain 2018" src="images/autos/sierraallterrain2018.jpg"></a>
						 <h1> <strong> <em> <font color="#000000" SIZE=6 > GMC  </font>  </em> </strong></h1> </font> 
						 <h2> <strong> <em> <font color="#000000"> SIERRA AT <sup>&reg;</sup> 2019</font> </em> </strong></h2> </font>  
						 <a class="read-more" href="pdfs/sierra2018.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                  </center>
 				 </div>

 				 <div class="item news-item">
	                 <center>
						 <a href="pdfs/sierra2018.pdf" target="_blank"><img alt="Sierra All Denali 2018" src="images/autos/sierradenali2018.jpg"></a>
						 <h1> <strong> <em> <font color="#000000" SIZE=6 > GMC  </font>  </em> </strong></h1> </font> 
						 <h2> <strong> <em> <font color="#000000"> SIERRA DENALI <sup>&reg;</sup> 2019</font> </em> </strong></h2> </font>  
						 <a class="read-more" href="pdfs/sierra2018.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                  </center>
 				 </div>

 				 <div class="item news-item">
	                 <center>
						 <a href="pdfs/encore2019.pdf" target="_blank"><img alt="Encore 2018" src="images/autos/encore2019.jpg"></a>
						 <h1> <strong> <em> <font color="#000000" SIZE=6 > BUICK  </font>  </em> </strong></h1> </font> 
						 <h2> <strong> <em> <font color="#000000"> ENCORE <sup>&reg;</sup> 2019</font> </em> </strong></h2> </font>  
						 <a class="read-more" href="pdfs/encore2019.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                  </center>
 				 </div>

 				 <div class="item news-item">
	                 <center>
						 <a href="pdfs/envision2019.pdf" target="_blank"><img alt="Envision 2018" src="images/autos/envision2019.jpg"></a>
						 <h1> <strong> <em> <font color="#000000" SIZE=6 > BUICK  </font>  </em> </strong></h1> </font> 
						 <h2> <strong> <em> <font color="#000000"> ENVISION <sup>&reg;</sup> 2019</font> </em> </strong></h2> </font>  
						 <a class="read-more" href="pdfs/envision2019.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                  </center>
 				 </div>

 				 <div class="item news-item">
	                 <center>
						 <a href="pdfs/enclave2019.pdf" target="_blank"><img alt="Enclave 2018" src="images/autos/enclave2019.jpg"></a>
						 <h1> <strong> <em> <font color="#000000" SIZE=6 > BUICK  </font>  </em> </strong></h1> </font> 
						 <h2> <strong> <em> <font color="#000000"> ENCLAVE <sup>&reg;</sup> 2019</font> </em> </strong></h2> </font>  
						 <a class="read-more" href="pdfs/enclave2019.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                  </center>
 				 </div>

 				 <div class="item news-item">
	                 <center>
						 <a href="pdfs/envision2018.pdf" target="_blank"><img alt="Envision 2019" src="images/autos/envision2018.jpg"></a>
						 <h1> <strong> <em> <font color="#000000" SIZE=6 > BUICK  </font>  </em> </strong></h1> </font> 
						 <h2> <strong> <em> <font color="#000000"> ENVISION <sup>&reg;</sup> 2019</font> </em> </strong></h2> </font>  
						 <a class="read-more" href="pdfs/envision2018.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                  </center>
 				 </div>

 				 <div class="item news-item">
	                 <center>
						 <a href="pdfs/ats2019.pdf" target="_blank"><img alt="ATS 2018" src="images/autos/ats2019.jpg"></a>
						 <h1> <strong> <em> <font color="#000000" SIZE=6 > CADILLAC  </font>  </em> </strong></h1> </font> 
						 <h2> <strong> <em> <font color="#000000"> ATS<sup>&reg;</sup> 2019</font> </em> </strong></h2> </font>  
						 <a class="read-more" href="pdfs/ats2019.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                  </center>
 				 </div>

 				 <div class="item news-item">
	                 <center>
						 <a href="pdfs/cts2019.pdf" target="_blank"><img alt="ATS 2018" src="images/autos/cts2019.jpg"></a>
						 <h1> <strong> <em> <font color="#000000" SIZE=6 > CADILLAC  </font>  </em> </strong></h1> </font> 
						 <h2> <strong> <em> <font color="#000000"> CTS<sup>&reg;</sup> 2019</font> </em> </strong></h2> </font>  
						 <a class="read-more" href="pdfs/cts2019.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                  </center>
 				 </div>

 				 <div class="item news-item">
	                 <center>
						 <a href="pdfs/escalade2019.pdf" target="_blank"><img alt="Escalade 2018" src="images/autos/escalade2019.jpg"></a>
						 <h1> <strong> <em> <font color="#000000" SIZE=6 > CADILLAC  </font>  </em> </strong></h1> </font> 
						 <h2> <strong> <em> <font color="#000000"> ESCALADE<sup>&reg;</sup> 2019</font> </em> </strong></h2> </font>  
						 <a class="read-more" href="pdfs/escalade2019.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                  </center>
 				 </div>


 				 <div class="item news-item">
	                 <center>
						 <a href="pdfs/xt52019.pdf" target="_blank"><img alt="XT5 2018" src="images/autos/xt52019.jpg"></a>
						 <h1> <strong> <em> <font color="#000000" SIZE=6 > CADILLAC  </font>  </em> </strong></h1> </font> 
						 <h2> <strong> <em> <font color="#000000"> XT5 <sup>&reg;</sup> 2019</font> </em> </strong></h2> </font>  
						 <a class="read-more" href="pdfs/xt52019.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                  </center>
 				 </div>
           
                </div>

                <br>

			</div>
            <!--FIN CONTENEDOR COCHES-->