<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>Servicio Buick®</title>
	<?php include('contenido/head.php'); ?>
</head>
<body>

	<?php include('chat.php'); ?>

	<!-- Container -->
	<div id="container">
    		<?php include('contenido/header.php'); ?>
        	<?php include('contenido/analytics.php'); ?>

			<div class="page-banner">
				<div class="container">
					<h2>Servicio Buick® FAME Manantiales</h2>

				</div>
			</div>


			<!-- contact box -->

				<div class="welcome-box">
					<div class="container">
                    
                    <p align="justify">
                    	<strong><h2>Servicios Incluidos Buick</h2></strong>
                        
TOMA VENTAJA DE ESTE NUEVO BENEFICIO PARA LOS VEHÍCULOS BUICK AÑO MODELO 2017.<br><br>

En Buick nos preocupamos por ti; es por ello que te otorgamos el NUEVO programa de Servicios Incluidos.<br><br>

Este programa aplica para los vehículos año modelo 2017, y te otorga un beneficio de 2 años o 24,000 km de Servicios Incluidos (lo que ocurra primero) con base en la póliza de garantía y programa de mantenimiento de tu vehículo.<br><br>

Cada servicio incluye cambio de filtro y aceite y rotación de ruedas; adicional incluye cambio de filtro de aire en el servicio de 24,000 km. Los servicios básicos incluyen aceite de motor sintético DEXOS, mano de obra y partes originales de acuerdo con las operaciones marcadas en la póliza de garantía y programa de mantenimiento de tu vehículo.<br><br>

Para mayor información visita tu Distribuidor Autorizado BUICK. Centro de Atención a Clientes 01 800 466 0818<br><br>
                    
                    
                    
                    </p>
                    
            <h3>Mantenimiento unidades 2014 y 2016</h3>
							<div class="single-project-content">
								<img alt="" src="images/serv-basico-buick2016.jpg">
                         	</div>                    
						
						<p align="justify">A partir de los modelos 2014 la frecuencia de servicio básico es cada 10,000 km o 12 meses, lo que ocurra primero. Los servicios arriba indicados incluyen aceite de motor SINTÉTICO DEXOS, refacciones originales y mano de obra de acuerdo con el programa de mantenimiento de la póliza del vehículo. Todos los precios incluyen IVA. Válidos en la República Mexicana del 01 de enero al 30 de junio de 2016. Precios sugeridos por General Motors de México, S. de R.L. de C.V. D.R., © General Motors de México, S. de R.L. de C.V., Av. Ejército Nacional 843, Col. Granada, 11520, México D.F. 2016</p><br><br><br><br>
 
 
            <h3>Mantenimiento unidades 2013 y anteriores</h3>
							<div class="single-project-content">
								<img alt="" src="images/serv-basico-buick2013.jpg">
                         	</div>                    
						
						<p align="justify">Los servicios arriba indicados incluyen aceite de motor SINTÉTICO DEXOS, refacciones originales y mano de obra de acuerdo con el programa de mantenimiento de la póliza del vehículo. Todos los precios incluyen IVA. Válidos en la República Mexicana del 01 de enero al 30 de junio de 2016. Precios sugeridos por General Motors de México, S. de R.L. de C.V. D.R., © General Motors de México, S. de R.L. de C.V., Av. Ejército Nacional 843, Col. Granada, 11520, México D.F. 2016</p><br><br><br><br>
                        
                        
            <h3>Mantenimiento de frenos</h3>
							<div class="single-project-content">
								<img alt="" src="images/serv-frenos-buick.jpg">
                         	</div>                    
						
						<p align="justify">Precios incluyen aceite de motor sintético Dexos, refacciones, mano de obra e IVA. Válido en la República Mexicana del 1° de Julio de 2016 al 31 de Diciembre de 2016. Precios sugeridos por General Motors de México, S. de R.L. de C.V. y sujetos a cambio sin previo aviso. Para información de otros vehículos, aplicabilidad o de las garantías correspondientes a los productos y servicios pregunte a su distribuidor Autorizado Buick-GMC D.R (C) General Motors de México, S. de R.L. de C.V., Av. Ejército Nacional 843, Col. Granada, 11520, Ciudad de México D.F, 2016.</p><br><br><br><br>
                         
                         <h3>Cambio de Aceite y Filtro</h3>
							<div class="single-project-content">
								<img alt="" src="images/aceite-filtro.jpg">
                         	</div>                    
						
						<p align="justify">Precios incluyen aceite de motor sintético Dexos, refacciones, mano de obra e IVA. Válido en la República Mexicana del 1° de Julio de 2016 al 31 de Diciembre de 2016. Precios sugeridos por General Motors de México, S. de R.L. de C.V. y sujetos a cambio sin previo aviso. Para información de otros vehículos, aplicabilidad o de las garantías correspondientes a los productos y servicios pregunte a su distribuidor Autorizado Buick-GMC D.R (C) General Motors de México, S. de R.L. de C.V., Av. Ejército Nacional 843, Col. Granada, 11520, Ciudad de México D.F, 2016.</p><br><br><br><br>
                        
                        <h2>Legales</h2>
                        <p align="justify">
                        <strong>LEGALES – SERVICIOS INCLUIDOS – BUICK 17MY</strong><br><br>

Los vehículos Buick año modelo 2017 cuentan con el Programa de Servicios Incluidos (“Programa SI”), el cual comprende únicamente los primeros 2 servicios de mantenimiento sin costo, es decir, el primer servicio sin costo aplica a los 12,000 kilómetros o 12 meses, lo que ocurra primero, y el segundo servicio sin costo aplica a los 24,000 kilómetros o 24 meses, lo que ocurra primero, y siempre y cuando los mismos se lleven a cabo conforme a lo establecido en los términos, condiciones, restricciones, operaciones e intervalos indicados en la Póliza de Garantía y Programa de Mantenimiento Buick. Los servicios sin costo del Programa SI antes indicados incluyen refacciones originales y mano de obra de acuerdo con el programa de mantenimiento Buick 2017, referido en la Póliza de Garantía y Programa de Mantenimiento Buick. El Programa SI es válido sólo en los Estados Unidos Mexicanos. El Programa SI no contempla el servicio de mantenimiento de 6,000 kilómetros recomendado bajo ciertas condiciones detalladas en la Póliza de Garantía y Programa de Mantenimiento Buick. GMM se reserva el derecho cancelar o de hacer cambios al Programa SI, en cualquier momento y sin previo aviso. Para mayor información del Programa SI consulte términos y condiciones con su Distribuidor Autorizado Buick previo a la compra de su vehículo Buick.<br><br>
                        
                        
                        
                        </p>

                	</div>
				</div>

		</div>
		
<?php include('contenido/footer.php'); ?>

</body>
</html>