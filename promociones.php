<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>Promociones</title>
	<?php include('contenido/head.php'); ?>
</head>
<body>

	<?php include('chat.php'); ?>

	<!-- Container -->
	<div id="container">
    <?php include('contenido/header.php'); ?>
        <?php include('contenido/analytics.php'); ?>
		<div id="content">

			<!-- Page Banner -->
			<div class="page-banner">
				<div class="container">
					<h2>PROMOCIONES</h2>
		
				</div>
			</div>

<!--			<div class="portfolio-box with-3-col">-->
				<div class="container">
						<div class="row">
							                


					<div class="portfolio-container">

						<div class="col-md-4">
                        <div class="work-post auto">
							<div class="work-post-gal">
								<a href="promos-buick.php" target="_self"><img alt="" src="images/buick-logo.png"></a>
							</div>
							<div class="work-post-content">
								<h5>Buick®</h5>
								<span><a href="promos-buick.php" target="_blank">Promociones del mes</a></span>
							</div>
						</div></div>
                        
						<div class="col-md-4">
                        <div class="work-post auto">
							<div class="work-post-gal">
								<a href="promos-gmc.php" target="_self"><img alt="" src="images/gmc-logo.png"></a>
							</div>
							<div class="work-post-content">
								<h5>GMC®</h5>
								<span><a href="promos-gmc.php" target="_blank">Promociones del mes</a></span>
							</div>
						</div></div>
                        
                        <div class="col-md-4">
						<div class="work-post auto">
							<div class="work-post-gal">
								<a href="promos-cadillac.php" target="_self"><img alt="" src="images/cadillac-logo.png"></a>
							</div>
							<div class="work-post-content">
								<h5>Cadillac®</h5>
								<span><a href="promos-cadillac.php" target="_blank">Promociones del mes</a></span>
							</div>
						</div>  </div>                                              
                        
                    
					</div>
				</div>
			</div>




		</div>

<?php include('contenido/footer.php'); ?>

 </body>
</html>