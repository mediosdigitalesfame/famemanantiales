<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>Servicio GMC®</title>
	<?php include('contenido/head.php'); ?>
</head>
<body>

	<?php include('chat.php'); ?>

	<!-- Container -->
	<div id="container">   
		<?php include('contenido/header.php'); ?>
        <?php include('contenido/analytics.php'); ?>
		
			<div class="page-banner">
				<div class="container">
					<h2>Servicio GMC® FAME Manantiales</h2>

				</div>
			</div>


			<!-- contact box -->

				<div class="welcome-box">
					<div class="container">
                    
                    <p align="justify">
                   <strong><h2> SERVICIOS INCLUIDOS GMCSERVICIOS INCLUIDOS GMC</h2></strong><br><br>

TOMA VENTAJA DE ESTE NUEVO BENEFICIO PARA LOS VEHÍCULOS GMC AÑO MODELO 2017.<br><br>
 
En GMC nos preocupamos por darte un servicio Professional Grade; es por ello que te otorgamos el NUEVO programa de Servicios Incluidos.<br><br>
 
Este programa aplica para los vehículos año modelo 2017, y te otorga un beneficio de 2 años o 24,000 km de Servicios Incluidos (lo que ocurra primero) con base en la póliza de garantía y programa de mantenimiento de tu vehículo.<br><br>
 
Cada servicio incluye cambio de filtro y aceite y rotación de ruedas; adicional incluye cambio de filtro de aire en el servicio de 24,000 km. Los servicios básicos incluyen aceite de motor sintético DEXOS, mano de obra y partes originales de acuerdo con las operaciones marcadas en la póliza de garantía y programa de mantenimiento de tu vehículo.
Para mayor información visita tu Distribuidor Autorizado GMC. Centro de Atención a Clientes 01 800 466 0801.<br><br>
                    
                    
                    
                    </p>
                    
           
							<div class="single-project-content">
								<img alt="" src="images/serv-basico-gmc2016.jpg">
                         	</div>                    
						
						<p align="justify">Precios incluyen aceite de motor SINTÉTICO DEXOS, refacciones originales y mano de obra de acuerdo con el programa de mantenimiento de póliza del vehículo. Todos los precios incluyen IVA, válidos en la República Mexicana del 1 de Junio de 2016 al 31 de diciembre de 2016. Precios sugeridos por General Motors de México S. de R.L. de CV. y sujetos a cambio sin previo aviso. Para información de otros vehículos, aplicabilidad o de las garantías correspondientes a los productos y servicios pregunte a su Distribuidor Autorizado GMC D.R. (C) General Motors de México de R.L. de C.V., Av. Ejército Nacional 843, Col. Granada, C.P. 11520, México D.F. 2016.</p><br><br><br><br>
 
 
           
							<div class="single-project-content">
								<img alt="" src="images/serv-basico-gmc2013.jpg">
                         	</div>                    
						
						<p align="justify">Precios incluyen aceite de motor SINTÉTICO DEXOS, refacciones originales y mano de obra de acuerdo con el programa de mantenimiento de póliza del vehículo. Todos los precios incluyen IVA, válidos en la República Mexicana del 1 de Junio de 2016 al 31 de diciembre de 2016. Precios sugeridos por General Motors de México S. de R.L. de CV. y sujetos a cambio sin previo aviso. Para información de otros vehículos, aplicabilidad o de las garantías correspondientes a los productos y servicios pregunte a su Distribuidor Autorizado GMC D.R. (C) General Motors de México de R.L. de C.V., Av. Ejército Nacional 843, Col. Granada, C.P. 11520, México D.F. 2016.</p><br><br><br><br>
                        
                        
        
							<div class="single-project-content">
								<img alt="" src="images/serv-frenos-gmc.jpg">
                         	</div>                    
						
						<p align="justify">Precios incluyen aceite de motor SINTÉTICO DEXOS, refacciones originales y mano de obra de acuerdo con el programa de mantenimiento de póliza del vehículo. Todos los precios incluyen IVA, válidos en la República Mexicana del 1 de Junio de 2016 al 31 de diciembre de 2016. Precios sugeridos por General Motors de México S. de R.L. de CV. y sujetos a cambio sin previo aviso. Para información de otros vehículos, aplicabilidad o de las garantías correspondientes a los productos y servicios pregunte a su Distribuidor Autorizado GMC D.R. (C) General Motors de México de R.L. de C.V., Av. Ejército Nacional 843, Col. Granada, C.P. 11520, México D.F. 2016.</p><br><br><br><br>
                        
                        
                        <p align="justify">
                     <h2><strong>LEGALES – SERVICIOS INCLUIDOS – GMC 17MYLEGALES – SERVICIOS INCLUIDOS – GMC 17MY</strong></h2><br><br>

Los vehículos GMC año modelo 2017 cuentan con el Programa de Servicios Incluidos (“Programa SI”), el cual comprende únicamente los primeros 2 servicios de mantenimiento sin costo, es decir, el primer servicio aplica sin costo a los 12,000 kilómetros o 12 meses, lo que ocurra primero, y el segundo servicio sin costo aplica a los 24,000 kilómetros o 24 meses, lo que ocurra primero, siempre y cuando los mismos se lleven a cabo conforme a lo establecido en los términos, condiciones, restricciones, operaciones e intervalos indicados en la Póliza de Garantía y Programa de Mantenimiento GMC. Los servicios sin costo del Programa SI antes indicados incluyen refacciones originales y mano de obra de acuerdo con el programa de mantenimiento GMC modelos 2017, referido en la Póliza de Garantía y Programa de Mantenimiento GMC. El Programa SI es válido sólo en los Estados Unidos Mexicanos. El Programa SI no contempla el servicio de 6,000 kilómetros recomendado bajo ciertas condiciones detalladas en la Póliza de Garantía y Programa de Mantenimiento GMC. GMM se reserva el derecho de hacer cambios al Programa SI, en cualquier momento y sin previo aviso. Para mayor información consulte términos y condiciones con su Distribuidor Autorizado GMC previo a la compra de su vehículo GMC.<br><br>
                        </p>

                	</div>
				</div>

		</div>
		<!-- End content -->

<?php include('contenido/footer.php'); ?>

</body>
</html>