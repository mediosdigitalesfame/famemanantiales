<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>GMC Acadia 2017</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="Title" content="FAME Manantiales, Morelia y Uruapan, Michoacán.">
	<meta name="description" content="Te brindamos información sobre las mejores marcas Premium del mercado. Buick, GMC y Cadillac te invitan a conocer exclusiva gama de vehículos de lujo y todos los servicios que ofrecemos para tí. Piensa en auto, piensa en FAME. Sitio WEB oficial FAME Manantiales Morelia y Uruapan. ">
    <meta name="keywords" content="FAME Manantiales Morelia, Nueva Acadia 2017, acadia, 2017, Acadia 2017, xt5, Nueva Cadillac XT5, fame manantiales uruapan, buick morelia, buick michoacan, buick mexico, buick manantiales, agencia buick morelia, agencia buick uruapan, seminuevos buick morelia, usados buick morelia, usados buick uruapan, enclave morelia, enclave uruapan, enclave 2016 morelia, enclave 2016 uruapan, lacrosse morelia, buick lacrosse morelia, buick lacrosse uruapan, buick encore morelia, buick encore uruapan, encore morelia, encore uruapan, encore, lacrosse, enclave, verano, regal, verano morelia 2016, buick verano morelia, buick verano uruapan, buick verano 2016 mexico, regal morelia, regal uruapan, buick regal morelia, buick regal uruapan, buick regal seminuevo, buick regal usado, buick verano seminuevo, buick verano usado, buick lacrosse seminuevo, buick lacrosse usado, buick enclave seminuevo, buick enclave usado, buick encore nuevo, buick encore seminuevo, buick encore usado, buick encore demo, buick oferta, buick promociones, buick taller, buick lujo, buick 2016, buick equipamiento, buick caracteristicas, ofertas buick, buick seminuevo morelia, buick seminuevo uruapan, buick seminuevos, buick agencia morelia, buick agencia uruapan, concesionario buick, concesionario buick morelia, concesionario buick uruapan, buick morelia telefono, buick morelia ubicacion, buick morelia mapa, buick morelia fame, buick fame, buick grupo fame, buick manantiales, buick manantiales morelia, buick manantiales uruapan, agencia gmc morelia, agencia gmc uruapan, autos gmc morelia, coches gmc morelia, camioneta gmc morelia, camionetas gmc morelia, camionetas gmc usados, camionetas gmc nuevos, gmc lujo morelia, autos de lujo morelia, precio gmc, precio buick, gmc acadia, gmc acadia denali, gmc sierra, gmc sierra regular, gmc sierra crew cab, gmc sierra denali, gmc sierra 2016, gmc terrain, gmc terrain morelia, gmc terrain denali, gmc terrain usada, camioneta terrain morelia, compro terrain morelia, gmc yukon, camioneta yukon, gmc camioneta yukon, gmc yukon 2016, gmc yukon caracteristicas, gmc yukon nueva, gmc yukon usada, general motors morelia, general motors uruapan, general motors mapa general motors telefono, agencia general motors, cadillac morelia, cadillac uruapan, cadillac 2016 morelia, cadillac 2016 uruapan, cadillac seminuevos morelia, cadillac usados morelia, cadillac morelia precio, cadillac morelia precio, cadillac onstar, buick onstar, gmc onstar, cadillac shield, cadillac ats morelia, cadillac ats, cadillac ats 2016, cadillac ats coupe, cadillac ats sedan, cadillac ats uruapan, cadillac nuevo, cadillac ats nuevo, cadillac ats seminuevo, cadillac cts, cadillac cts morelia, cadillac cts precio, cadillac cts promocion, promociones cadillac, bonos cadillac, cadillac escalade nueva, cadillac escalade 2016, cadillac escalade apatzingan, cadillac escalade uruapan, cadillac escalade usada, cadillac escalade negra, cadillac escalade seminueva, cadillac srx, cadillac srx morelia, cadillac srx uruapan, grupo fame manantiales, grupo fame cadillac, grupo fame buick, grupo fame gmc, fame manantiales, fame cadillac, fame buick, fame gmc, fame manantiales uruapan, fame manantiales ubicacion, fame manantiales telefono, fame manantiales mapa, agencia fame manantiales, fame manantiales taller, fame manantiales servicio, servicio cadillac morelia, servicio buick morelia, servicio autos de lujo, servicio autos de lujo morelia, morelia, uruapan, michoacan, mexico">
    <meta name="author" content="Mercadotecnia Grupo FAME División Automotriz">
    
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,700,600,300' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="../css/bootstrap.css" type="text/css" media="screen">
	<link rel="stylesheet" type="text/css" href="../css/magnific-popup.css" media="screen">
	<link rel="stylesheet" type="text/css" href="../css/owl.carousel.css" media="screen">
    <link rel="stylesheet" type="text/css" href="../css/owl.theme.css" media="screen">
	<link rel="stylesheet" type="text/css" href="../css/jquery.bxslider.css" media="screen">
	<link rel="stylesheet" type="text/css" href="../css/font-awesome.css" media="screen">
	<link rel="stylesheet" type="text/css" href="../css/style.css" media="screen">
	<link rel="stylesheet" type="text/css" href="../css/flexslider.css" media="screen">
	<link rel="stylesheet" type="text/css" href="../css/responsive.css" media="screen">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <link rel="icon" type="image/png" href="../images/favicon.png" />

</head>
<body<em></em>

	<!-- Container -->
	<div id="container">
		<!-- Header -->
		<header class="clearfix">
			<!-- Static navbar -->
			<div class="navbar navbar-default navbar-fixed-top">
				<div class="top-line">
					<div class="container">
						<p>
							<span><i class="fa fa-phone"></i>Agencia: (443) 334 4440</span>
						</p>
						<ul class="social-icons">
							<li><a class="facebook" href="https://www.facebook.com/CadillacBuickYGmc?fref=ts" target="_blank"><i class="fa fa-facebook"></i></a></li>
							<li><a class="twitter" href="https://twitter.com/manantialesf" target="_blank"><i class="fa fa-twitter"></i></a></li>
							<li><a class="youtube" href="https://www.youtube.com/user/GrupoFameAutos" target="_blank"><i class="fa fa-youtube"></i></a></li>
                            <li><a class="whatsapp" href="http://www.grupofame.com/whatsapp/" target="_blank"><i class="fa fa-whatsapp"></i></a></li>
                            <li><a class="instagram" href="https://www.instagram.com/grupofame" target="_blank"><i class="fa fa-instagram"></i></a></li>
                           	
						</ul>
					</div>
				</div>
                
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="index.html" target="_self"><img alt="Inicio" src="../images/logo.png"></a>
					</div>
					<div class="navbar-collapse collapse">
						<ul class="nav navbar-nav navbar-right">

							<li><a href="../index.html">Inicio</a></li>
							<li class="drop"><a class="active" href="../autos.html">Vehículos</a>
								<ul class="drop-down">
									<li><a href="../fichas/enclave.pdf" target="_self">Buick Enclave</a></li>
									<li><a href="../fichas/encore.pdf" target="_self">Buick Encore</a></li>
									<li><a href="../fichas/lacrosse.pdf" target="_self">Buick LaCrosse</a></li>
									<li><a href="../fichas/regal-gs.pdf" target="_self">Buick Regal GS</a></li>
									<li><a href="../fichas/verano.pdf" target="_self">Buick Verano</a></li>	
									<li><a href="../fichas/ats-coupe.pdf" target="_self">Cadillac ATS Coupé</a></li>
                                    <li><a href="../fichas/ats-sedan.pdf" target="_self">Cadillac ATS</a></li>	
									<li><a href="../fichas/cts.pdf" target="_self">Cadillac CTS</a></li>
									<li><a href="../fichas/escalade.pdf" target="_self">Cadillac Escalade</a></li>
									<li><a href="../fichas/srx.pdf" target="_self">Cadillac SRX</a></li>
									<li><a href="../gmc-acadia2017/index.php" target="_self">GMC Acadia</a></li>
									<li><a href="../fichas/acadia-denali.pdf" target="_self">GMC Acadia Denali</a></li>	
									<li><a href="../fichas/sierra-denali.pdf" target="_self">GMC Sierra Denali</a></li>
                                    <li><a href="../fichas/sierra-all.pdf" target="_self">GMC Sierra Crew Cab</a></li>
									<li><a href="../fichas/sierra-all.pdf" target="_self">GMC Sierra</a></li>
									<li><a href="../fichas/terrain.pdf" target="_self">GMC Terrain</a></li>	
									<li><a href="../fichas/terrain.pdf" target="_self">GMC Terrain Denali</a></li>
                                    <li><a href="../fichas/yukon.pdf" target="_self">GMC Yukon</a></li>
                                </ul> </li>    

							<li class="drop"><a href="../servicio.php">Servicio</a>
								<ul class="drop-down">
									<li><a href="../servicio.php">Cita de Servicio</a></li>
                                    <li><a href="../servicio-buick.html" target="_self">Servicio Buick</a></li>
                                    <li><a href="../servicio-gmc.html" target="_self">Servicio GMC</a></li>
                                    <li><a href="../servicio-cadillac.html" target="_self">Servicio Cadillac</a></li>
                                    <li><a href="../refacciones.php">Refacciones</a></li>
                                    
                                  </ul> </li> 
                                  
							<li class="drop"><a href="#">Beneficios</a>
								<ul class="drop-down">
									<li><a href="../garantia-buick.html">Garantía Buick</a></li>
                                    <li><a href="../garantia-gmc.html" target="_self">Garantía GMC</a></li>
                                    <li><a href="../garantia-cadillac.html" target="_self">Garantía Cadillac</a></li>
                                    <li><a href="../onstar.html">On Star®</a></li>
                                    <li><a href="../cadillac-shield.html">Cadillac Shield®</a></li>
                                  </ul> </li>                                   
                                  
                        
							<li class="drop"><a href="../promociones.html">Promociones</a>
                            	<ul class="drop-down">
                                	<li><a href="../promos-buick.html">Buick®</a></li>
                                	<li><a href="../promos-gmc.html">GMC®</a></li>
                                	<li><a href="../promos-cadillac.html">Cadillac®</a></li>
                                    <li><a href="../gm-financial.html">GM® Financial</a></li>
                            	</ul></li>   
                                
                                                 
							<li class="drop"><a href="../contacto.php">Contacto</a>
                            	<ul class="drop-down">
                                	<li><a href="../contacto.php">Morelia</a></li>
                                    <li><a href="../contacto-upn.php">Uruapan</a></li>
                                    <li><a href="../manejo.php">Cita de Manejo</a></li>
                                    <li><a href="../cotiza.php">Cotizador de auto</a></li>
                                  
                                </ul></li>
							<li><a href="../ubicacion.html">Ubicación</a></li>
                        </ul>
                        
					</div>
				</div>
			</div>
		</header>
		<!-- End Header -->
        
<!-- ANALYTICS MANANTIALES-->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-47755725-1', 'auto');
  ga('send', 'pageview');

</script>

<!-- FIN ANALYTICS -->


			<!-- Page Banner -->
			<div class="page-banner">
				<div class="container">
					<h2>GMC Acadia 2017</h2>

				</div>
			</div>


			<!-- contact box -->
			<div class="contact-box">
				<div class="container">
					<div class="row">
<div class="single-project-content">
<div class="col-md-12">
	<img src="../images/acadia-img1.jpg" alt="Acadia 2017">
</div></div>

 				<div class="col-md-6">
					<div class="services-post">
						<a class="services-icon2" href="../fichas/acadia-2017.pdf" target="_blank"><i class="fa fa-download"></i></a>
							<div class="services-post-content">
										<h4>+Información</h4>
										<p>Conoce más sobre la nueva Acadia 2017<br> descarga su <strong><a href="../fichas/acadia-2017.pdf" target="_blank">ficha técnica</a></strong></p><br>
                                        <p align="justify"><br>
                                        
                                    <h3> Acadia SLT1 FWD   -   Precio desde $680,900*<br>
                                    	Acadia Denali AWD  -  Precio desde  $741,600*
                                  	 </h3>

											
										· Motor de 3.6L SIDI V6 con 310 hp<br>
                                        · Suspensión dinámica Flexride con modo normal FWD, normal AWD, sport, Off Road/All Terrain, y arrastre.<br>
										· Torque: 271 lb-ft @ 5,000 rpm<br>
										· Transmisión automática de 6 velocidades AWD de control electrónico<br>
                                        
                                        
                                        </p>                                        
                                        
					  </div>
				  </div>
							</div>
                                                      
                            
					  <div class="col-md-6">
						  <h3>Cotiza tu Nueva Acadia 2017</h3>
                            
<?php
	if (isset($_POST['boton'])) {
        if($_POST['nombre'] == '') {
        	$errors[1] = '<span class="error">Ingrese su nombre</span>';
        } else if($_POST['email'] == '' or !preg_match("/^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/",$_POST['email'])){
        	$errors[2] = '<span class="error">Ingrese un email correcto</span>';
        } else if($_POST['telefono'] == '') {
        	$errors[3] = '<span class="error">Ingrese un teléfono</span>';
        } else if($_POST['mensaje'] == '') {
        	$errors[4] = '<span class="error">Ingrese un mensaje</span>';
        } else {
        	$dest = "ventas@famemanantiales.com, gerencia@famemanantiales.com" . ','. "formas@grupofame.com" . ',' . ",";
            $nombre = $_POST['nombre'];
            $email = $_POST['email'];
            $telefono = $_POST['telefono'];
			$asunto_cte = "Cotizador Acadia 2017 Fame Manantiales GMC, Buick y Cadillac";
			$asunto = "Cotizador Acadia 2017 Fame Manantiales GMC, Buick y Cadillac";
            $cuerpo = $_POST['mensaje'];
			$cuerpo_mensaje .= $nombre . "<br>" . "Mensaje: ". $cuerpo . "<br>" . "Mi correo es: " . $_POST['email'] . "<br>" . "Mi teléfono es: " . $_POST['telefono'];
			$cuerpo_cte = '
			<html>
			<head>
			  <title>Mail from '. $nombre .'</title>
			</head>
			<body>
			  <table style="width: 500px; font-family: arial; font-size: 14px;" border="1">
				<tr style="height: 32px;">
				  <th align="right" style="width:150px; padding-right:5px;">Nombre:</th>
				  <td align="left" style="padding-left:5px; line-height: 20px;">'. $nombre .'</td>
				</tr>
				<tr style="height: 32px;">
				  <th align="right" style="width:150px; padding-right:5px;">Correo:</th>
				  <td align="left" style="padding-left:5px; line-height: 20px;">'. $email .'</td>
				</tr>
				<tr style="height: 32px;">
				  <th align="right" style="width:150px; padding-right:5px;">Mensaje:</th>
				  <td align="left" style="padding-left:5px; line-height: 20px;">'. $cuerpo .'</td>
				</tr>
			  </table>
			</body>
			</html>
			';
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
			$headers .= 'From: ' . $email . "\r\n";
            
            if(mail($dest,$asunto,$cuerpo_mensaje,$headers)){
            	$result = '<div class="result_ok">Email enviado correctamente, en breve nos comunicaremos contigo. Gracias por tu preferencia. "Piensa en Auto, Piensa en FAME"</div>';
				mail($email,$asunto_cte,$cuerpo_cte,$headers);
                $_POST['nombre'] = '';
                $_POST['email'] = '';
                $_POST['telefono'] = '';
                $_POST['mensaje'] = '';
            } else {
            	$result = '<div class="result_fail">Hubo un error al enviar el mensaje</div>';
            }
        }
    }
?>
<html>
	<meta charset="utf-8">
    <link rel='stylesheet'| href='../css/formularios.css'>
    <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js'></script>
	<form id="contact-form" class="contact-work-form2" method='post' action=''>
    	<div class="text-input">
        	<div class="float-input">
            <input name='nombre' id="nombre" placeholder="Nombre*" type='text' class='nombre' value='<?php if(isset($_POST['nombre'])){ echo $_POST['nombre']; } ?>'> 
        <?php if(isset($errors)){ echo $errors[1]; } ?><span><i class="fa fa-user"></i></span></div>
        
        <div class="float-input2"><input name='email' placeholder="E-mail*" type='text' class='email' value='<?php if(isset($_POST['email'])){ echo $_POST['email']; } ?>'>
        <?php if(isset($errors)){ echo $errors[2]; } ?><span><i class="fa fa-envelope"></i></span></div>
        </div>
        
        <div class="text-input">
        <div class="float-input">
        <input name='telefono' id="telefono" placeholder="Teléfono*" type='tel' class='telefono' value='<?php if(isset($_POST['telefono'])){ echo $_POST['telefono']; } ?>'>
        <?php if(isset($errors)){ echo $errors[3]; } ?><span><i class="fa fa-phone"></i></span>
        </div>
        </div>
        
        <div class="textarea-input"><textarea name='mensaje' placeholder="Mensaje*" rows='5' class='mensaje'><?php if(isset($_POST['mensaje'])){ echo $_POST['mensaje']; } ?></textarea>
        <?php if(isset($errors)){ echo $errors[4]; } ?><span><i class="fa fa-comment"></i></span>
        </div>
        <div><input name='boton' type='submit' value='Enviar' class='boton'></div>
        <?php if(isset($result)) { echo $result; } ?>
    </form>                            
                            
                           
                            
                    
					  </div>
                        
                        
                        
                        
<!-- Despues del Formulario.-->
<div class="container">
<div class="single-project-content">
	<h1>Descubre la nueva</h1>
		<h2>GMC ACADIA 2017</h2>
			
            <div class="col-md-3">
   			 <img src="../images/acadia-img2.jpg"><br>
    	 <p align="justify"><strong>Parrilla cromada</strong><br>
		Luce imponenete con la parrilla tipo panal en 3 bloques, con filo cromado.*</p>
   	</div>
 
    <div class="col-md-3">
    <img src="../images/acadia-img3.jpg"><br>
     	<p align="justify"><strong>Faros delanteros HID </strong><br>
						Emiten un potente haz luminoso y cuentan con tecnología Intellibeam.*
        										</p>
   	</div>
    
    <div class="col-md-3">
    <img src="../images/acadia-img4.jpg"><br>
   			 <p align="justify"><strong>Rines de Aluminio</strong> <br>
						La atractiva línea de diseño es impulsada por rines de aluminio de 20".*</p>
   	</div>
    
    <div class="col-md-3">
    <img src="../images/acadia-img5.1.jpg"><br>
    			<p align="justify"><strong>Detección de peatones</strong><br>
							Presenta un sistema de detección de peatones con frenado frontal peatonal.*</p>
                            
   	</div>
    <div class="legales">*Características unicamente del paquete Denali</div>
    <br><br><br>
    

</div></div>


<div class="container">
	<div class="single-project-content">
		<div class="col-md-8">
		<img src="../images/acadia-img6.jpg">
        </div>
     <div class="col-md-4">
     <p align="justify">
     					<br><br><br><h1>POTENTE MOTOR </h1><br>
                        <h3>Te llevará a donde quieras llegar</h3>
		· Motor de 3.6L SIDI V6 con 310 hp<br>
		· Torque: 271 lb-ft @ 5,000 rpm<br>
		· Suspensión dinámica Flexride con modo normal FWD, normal AWD, sport, Off Road/All Terrain, y arrastre.<br>
		· Suspensión de alto desempeño, delantera McPherson y trasera independiente de 5-link<br>
		· Control de tracción automático y electrónico<br>
 		· Transmisión automática de 6 velocidades AWD de control electrónico<br>
		· Sistema de nivel superior<br></p>
     
    </div>
	
</div></div>


<div class="container">
<div class="single-project-content">
 <div class="col-md-4">
     <p align="justify">
     	<h1>INTERIOR CON CLASE</h1><br>
              <h3>Es la nueva GMC Acadia Denali 2017, una SUV imponente en todo sentido.</h3>
		·Asientos delanteros calefactables.<br>
		·Asientos delanteros con ventilación, traseros calefactables y pulsaciones de seguridad en el asiento del conductor <br>
        ·Cluster de información para el conductor de 8" reconfigurable multicolor.<br>
        ·Volante forrado en piel de lujo con controles de audio y tléfono, calefactable.<br>
        ·Configuración de asientos para 6 pasajeros (2-2-2), segunda fila de asientos deslizables 40/40, tercera fila de asientos tipo banca abatibles 50/50. <br>
        <!--·Sistema de 8 bocinas BOSE® de alto desempeño con amplificador y subwoofer.<br>
        ·Radio con GMC Intellilink con pantalla táctil a color de 8" con Navegación en mapas 3D, Bluetooth para audio y teléfono, y nuevo Smartphone integration con Apple CarPlay® y Android Auto® con reconocimiento de voz. Con recepción HD para audio.--><br>
        
        
        
        </p>
     
    </div>
    <div class="col-md-8">
		<img src="../images/acadia-img7.jpg">
        </div>
	
</div></div>


<div class="container">
<div class="single-project-content">
 <div class="col-md-8">
		<img src="../images/acadia-img8.jpg">
        </div>
 
 <div class="col-md-4">
     <p align="justify">
     	<h1>SEGURIDAD </h1><br>
              <h3>La seguridad es primero</h3>
              
		· Bolsas de aire: frontales, laterales y de cortina para asientos delanteros y traseros, de rodilla para conductor y central<br>
		· Detector de ocupante para el asiento del copiloto.<br>
   		· Cinturones de seguridad de 3 puntos, con pretensores, sistema retractor y limitadores de carga, ajustable de altura y con aviso acústico de uso.<br>
		· Llanta de refacción de acero 135/70R18<br>
        · Sensores de reversa traseros montados en fascia, de detección de obstáculos laterales en espejos.<br>
       <!-- · Sistema antirrobo con alarma de sensores ultrasónicos, alarma de pánico, destello de faros y accionamiento de claxón.<br>
        · Sensores frontales montados en la fascia para facilitar las maniobras de estacionamiento por medio de detección de objetos.<br>
        · Sistema de destección de peatones con frenado automático: Utiliza Ia cámara frontal para escanear el camino, alertando al conductor de peatones. Si el conductor no aplica los frenos a tiempo, el sistema automaticamente lo hace.--><br>
        
        </p>
     
    </div>
    
	
</div></div>



<div class="container">
	<div class="single-project-content">
    <div class="col-md-12">
    	<h1>Colores </h1><br>
    </div>
    
    <div class="col-md-12">
    	<img src="../images/acadia-colores.jpg">
    </div>
    	<!--
        <div class="col-md-2">
        	<img src="../images/civic-img9.png"><br>
            <p align="justify">
            	<strong> ROJO CARMÍN</strong><br>
                                
			</p>
		</div>	
        
        <div class="col-md-2">
        	<img src="../images/civic-img10.png"><br>
            <p align="justify">
            	<strong>BLANCO PLATINO / BLANCO</strong><br>
      		</p>
		</div>	
        
         <div class="col-md-2">
        	<img src="../images/civic-img11.png"><br>
            <p align="justify">
            	<strong>NEGRO ONIX</strong><br>
              
			</p>
		</div>	
        
         <div class="col-md-2">
        	<img src="../images/civic-img12.png">
            <p align="justify">
            	<strong>PLATA BRILLANTE</strong><br>
               
			</p>
		</div>	
        
                  
          <div class="col-md-2">
        	<img src="../images/civic-img12.png">
            <p align="justify">
            	<strong>CHAMPAGNE MÉTALICO</strong><br>
               
			</p>
		</div>
        
          <div class="col-md-2">
        	<img src="../images/civic-img12.png">
            <p align="justify">
            	<strong>IRIDIO MÉTALICO</strong><br>
               
			</p>
		</div>-->
        
	</div><br><br>
    
    <p align="justify">
   * El precio del vehículo mostrado en esta página, no representa el precio de compraventa final. Precios sujetos a cambios sin previo aviso (Inflación, paridad con el USD, impactos en el mercado, etc.). No aplican otras promociones.<br> </p>
   
   <p align="justify">**Para mayor información y disponibilidad visite su agencia Fame Manantiales Morelia.</p>
   
   <div class="legales">Promoción válida para la República Mexicana del 02 de agosto al 01 de septiembre 2016. La renta mensual incluye I.V.A. Aplica para personas físicas y morales. Válido para GMC Acadia 2017 paquete (B) SLT 1 FWD anunciado en este material en planes de Arrendamiento Puro a 48 meses. No aplica con otras promociones. El plan de arrendamiento descrito consiste en una inversión inicial de $24,800 pesos M.N., la cual incluye un depósito en garantía. La renta mensual no incluye seguro contra daños, de vida, de desempleo y/o incapacidad total o temporal, gastos de placas, tenencia, comisión por apertura o cualquier otro impuesto o derecho aplicable, ni gastos de gestoría, los cuales serán pagados por el cliente adicionalmente a la renta mensual. Sujeto a aprobación de Arrendamiento Puro por GM Financial de México, S.A. de C.V., SOFOM, E.R. Acuda con su Distribuidor autorizado General Motors para mayor información sobre los planes de Arrendamiento Puro específicos, modelos participantes, modalidades, requisitos de contratación, comisiones, términos y condiciones. Los precios, colores, equipos y especificaciones técnicas están sujetos a cambios sin previo aviso o descontinuar la producción de un modelo determinado y están sujetos a disponibilidad de cada Distribuidor Autorizado General Motors. Las fotos y renta mensual son de carácter ilustrativo e informativo. Consulta con tu Distribuidor Autorizado General Motors para conocer la renta aplicable y vigente. Las marcas GMC®, Acadia® así como sus respectivos logotipos y avisos comerciales son propiedad de General Motors, LLC y General Motors de México, S. de R.L. de C.V. es su licenciataria autorizada en los Estados Unidos Mexicanos.<br>
Promoción válida para la República Mexicana del 02 de agosto al 01 de septiembre 2016. La renta mensual incluye I.V.A. Aplica para personas físicas y morales. Válido para GMC Acadia 2016 paquete (E) anunciado en este material en planes de Arrendamiento Puro a 48 meses. No aplica con otras promociones. El plan de arrendamiento descrito consiste en una inversión inicial de $29,400 pesos M.N., la cual incluye un depósito en garantía. La renta mensual no incluye seguro contra daños, de vida, de desempleo y/o incapacidad total o temporal, gastos de placas, tenencia, comisión por apertura o cualquier otro impuesto o derecho aplicable, ni gastos de gestoría, los cuales serán pagados por el cliente adicionalmente a la renta mensual. Sujeto a aprobación de Arrendamiento Puro por GM Financial de México, S.A. de C.V., SOFOM, E.R. Acuda con su Distribuidor autorizado General Motors para mayor información sobre los planes de Arrendamiento Puro específicos, modelos participantes, modalidades, requisitos de contratación, comisiones, términos y condiciones. Los precios, colores, equipos y especificaciones técnicas están sujetos a cambios sin previo aviso o descontinuar la producción de un modelo determinado y están sujetos a disponibilidad de cada Distribuidor Autorizado General Motors. Las fotos y renta mensual son de carácter ilustrativo e informativo. Consulta con tu Distribuidor Autorizado General Motors para conocer la renta aplicable y vigente. Las marcas GMC®, Acadia® así como sus respectivos logotipos y avisos comerciales son propiedad de General Motors, LLC y General Motors de México, S. de R.L. de C.V. es su licenciataria autorizada en los Estados Unidos Mexicanos.<br>
Precios sugeridos por el fabricante. El precio que aparece en este sitio puede diferir del precio ofrecido por el Distribuidor Autorizado GMC®. El precio del vehículo mostrado en esta página, no representa el precio de compraventa final. Precios sujetos a cambios sin previo aviso (Inflación, paridad con el USD, impactos en el mercado, etc). Vigencia del 02 de Agosto de 2016 al 31 de Agosto de 2016.</div>

<br><br>
        
    </div>

<div class="container">
	<div class="single-project-content">
    	<p align="justify">
        	<a href="../fichas/acadia-2017.pdf" target="_blank"><strong> Ver Ficha tecnica ACADIA 2017</strong></a>
        </p>
    </div>
</div>



<!--Fin del Contenido --->
					</div>
				</div>
			</div>

		</div><br><br><br><br><br>



		<!-- End content -->


		<!-- footer 
			================================================== -->
		<footer>
			<div class="footer-line">
				<div class="container">
					<p><span><i class="fa fa-phone"></i>  01800 670 8386 | </span> 2016 Fame Manantiales | <i class="fa fa-user"> </i><a href="../aviso.html"><font color="#FFFFFF"><strong>  Aviso de privacidad</strong></font></a></p>
					<a class="go-top" href="#"></a>
				</div>
			</div>

		</footer>
		<!-- End footer -->
        
	</div>
	<!-- End Container -->

	<script type="text/javascript" src="../js/jquery.min.js"></script>
	<script type="text/javascript" src="../js/jquery.migrate.js"></script>
	<script type="text/javascript" src="../js/bootstrap.js"></script>
  	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
	<script type="text/javascript" src="../js/gmap3.min.js"></script>
	<script type="text/javascript" src="../js/retina-1.1.0.min.js"></script>
	<script type="text/javascript" src="../js/plugins-scroll.js"></script>
	<script type="text/javascript" src="../js/script.js"></script>

	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
</body>
</html>