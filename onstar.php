<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>Servicio OnStar®</title>
	<?php include('contenido/head.php'); ?>
</head>
<body>

	<?php include('chat.php'); ?>

	<!-- Container -->
	<div id="container">
    <?php include('contenido/header.php'); ?>
        <?php include('contenido/analytics.php'); ?>
        
			<div class="page-banner">
				<div class="container">
					<h2>Servicio OnStar® FAME Manantiales</h2>

				</div>
			</div>


			<!-- contact box -->

				<div class="welcome-box">
					<div class="container">
                    
            <h3><strong>Mantenimiento unidades 2015 y 2014</strong></h3>
							<div class="single-project-content">
								<img alt="" src="images/onstar.png">
                         	</div>                    
						
<iframe width='100%' height='500px' frameborder="0" allowfullscreen src='https://www.youtube.com/embed/EywAb4QcaV8?rel=0&autoplay=1'></iframe>  <br>
<br><br>
                       
                        <h2>¿Qué es OnStar®?</h2>
						<p align="justify">El sistema más completo en seguridad, protección, navegación y conectividad de la industria.<br><br>
 
Desde 1996, OnStar ha proporcionado servicios únicos de seguridad a conductores y pasajeros en Estados Unidos y Canadá; y ha prestado, hasta el día de hoy, servicios a más de 6 millones de suscriptores.</p><br><br><br><br>


                        <h2>¿Cómo funciona</h2>
						<p align="justify"><strong>OnStar®</strong> es un sistema integrado en el vehículo.<br><br>
Es capaz de conectar al conductor con un Asesor en vivo, al presionar cualquiera de los 3 botones ubicados en el espejo retrovisor.<br><br>
Nuestros Asesores, especialmente capacitados, siempre estarán listos para atender su llamada las 24 horas del día y los 365 días del año.<br><br>
</p>
<p align="center">Para conocer más sobre esta tecnología única en México visite:<br>
<a href="https://www.onstar.com.mx/inicio.html">http://www.onstar.com.mx</a></p>
<br><br><br><br>
 
<h2><strong>Beneficios del servicio OnStar®</strong></h2>
				<!-- ENLACES -->
				<div class="services-box">
					<div class="container">
						<div class="row">
							<div class="col-md-6">
								<div class="services-post">
									<div class="services-post-content">
										<h4><a><strong>Respuesta Automática de Accidente</strong></a></h4>
										<p align="justify">Podemos enviarle ayuda incluso cuando usted no pueda solicitarla. Si alguna vez sufre o es espectador de un accidente, a través de este servicio, se alertará a un Asesor OnStar, quien le enviará ayuda a su ubicación exacta.</p>
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="services-post">
									<div class="services-post-content">
										<h4><a><strong>Servicios de Emergencia</strong></a></h4>
										<p align="justify">Con emergencias OnStar se otorga ayuda inmediata, ya sea una situación médica, desastre natural o si usted fue testigo de un accidente. Sólo presione el botón rojo y OnStar le asistirá y orientará.</p>
									</div>
								</div>
							</div>
                        

						</div>
					</div>


					<div class="container">
						<div class="row">
							<div class="col-md-6">
								<div class="services-post">
									<div class="services-post-content">
										<h4><a><strong>Servicios Remotos</strong></a></h4>
										<p align="justify">Si dejó las llaves dentro de su vehículo, no se preocupe. Una llamada a nuestro Call Center (presionando el botón) y un Asesor activará la Apertura de Puertas Remota.</p>
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="services-post">
									<div class="services-post-content">
										<h4><a><strong>Asistencia en Vehículos Robados</strong></a></h4>
										<p align="justify">Si reportó el robo de su vehículo a las autoridades competentes, OnStar, a través de tecnología GPS, puede precisar la ubicación exacta de su vehículo y proporcionarla a las autoridades correspondientes para ayudar en su recuperación.</p>
									</div>
								</div>
							</div>
                        

						</div>
					</div>  
                    
                    
                    
					<div class="container">
						<div class="row">
							<div class="col-md-6">
								<div class="services-post">
									<div class="services-post-content">
										<h4><a><strong>Navegación Paso a Paso</strong></a></h4>
										<p align="justify">Presione el botón azul de OnStar y un Asesor podrá enviarle direcciones paso a paso, directamente a su vehículo. Si alguna vez se desvía de su camino, el sistema automáticamente recalculará la ruta para asegurarse que llegue a su destino final.</p>
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="services-post">
									<div class="services-post-content">
										<h4><a><strong>Diagnóstico</strong></a></h4>
										<p align="justify">Reciba mensualmente un reporte de Diagnóstico de Vehículo OnStar por correo electrónico, con los resultados de las revisiones realizadas a su motor, transmisión, frenos y más. Si algo le preocupa mientras conduce, sólo presione el botón azul de OnStar y un Asesor podrá solicitar Diagnósticos On Demand para una rápida revisión remota de los principales sistemas operativos de su vehículo.</p>
									</div>
								</div>
							</div>
                        

						</div>
					</div>                                       
				<img class="shadow-image" alt="" src="images/shadow.png">
				</div>
                
               

			</div>



		</div>
		<!-- FIN ENLACES -->

   

                	</div>
				</div>

		</div>
		<!-- End content -->

<?php include('contenido/footer.php'); ?>

 </body>
</html>