<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>Autos</title>
	<?php include('contenido/head.php'); ?>
</head>
<body>

	<?php include('chat.php'); ?>

	<!-- Container -->
	<div id="container">
    <?php include('contenido/header.php'); ?>
        <?php include('contenido/analytics.php'); ?>
		<div id="content">

			<!-- Page Banner -->
			<div class="page-banner">
				<div class="container">
					<h2>Vehículos <strong>Buick · GMC · Cadillac</strong></h2>
		
				</div>
			</div>

			<div class="portfolio-box with-3-col">
				<div class="container">
					<ul class="filter">
						<li><a href="#" class="active" data-filter="*"><i class="fa fa-th"></i>Mostrar todos</a></li>
						<li><a href="#" data-filter=".auto">Autos</a></li>
						<li><a href="#" data-filter=".suv">SUV's - Minivan</a></li>
						<li><a href="#" data-filter=".pickup">Pick Up's</a></li>
                        
					</ul>

					<div class="portfolio-container">

						<div class="work-post suv">
							<div class="work-post-gal">
								<img alt="" src="images/autos/original/encore2019.jpg">
							</div>
							<div class="work-post-content">
								<h5>Buick® Encore 2019 </h5>
								<span><a href="pdfs/encore2019.pdf" target="_blank">Ficha técnica</a></span>
							</div>
						</div>

						<div class="work-post suv">
							<div class="work-post-gal">
								<img alt="" src="images/autos/original/envision2019.jpg">
							</div>
							<div class="work-post-content">
								<h5>Buick® Envision 2019 </h5>
								<span><a href="pdfs/envision2019.pdf" target="_blank">Ficha técnica</a></span>
							</div>
						</div> 
                        
						<div class="work-post suv">
							<div class="work-post-gal">
								<img alt="" src="images/autos/original/enclave2019.jpg">
							</div>
							<div class="work-post-content">
								<h5>Buick® Enclave 2019 </h5>
								<span><a href="pdfs/enclave2019.pdf" target="_blank">Ficha técnica</a></span>
							</div>
						</div> 

						<div class="work-post suv">
							<div class="work-post-gal">
								<img alt="" src="images/autos/original/envision2018.jpg">
							</div>
							<div class="work-post-content">
								<h5>Buick® Envision 2019 </h5>
								<span><a href="pdfs/envision2018.pdf" target="_blank">Ficha técnica</a></span>
							</div>
						</div>                        
                                 
					<!--	<div class="work-post auto">
							<div class="work-post-gal">
								<img alt="" src="images/autos/regal-gs.png">
							</div>
							<div class="work-post-content">
								<h5>Buick® Regal GS</h5>
								<span><a href="fichas/regal-gs.pdf" target="_blank">Ficha técnica</a></span>
							</div>
						</div>  -->       
                        
						<div class="work-post auto">
							<div class="work-post-gal">
								<img alt="" src="images/autos/original/ats2019.jpg">
							</div>
							<div class="work-post-content">
								<h5>Cadillac® ATS 2019</h5>
								<span><a href="pdfs/ats2019.pdf" target="_blank">Ficha técnica</a></span>
							</div>
						</div>
                        
						<div class="work-post suv">
							<div class="work-post-gal">
								<img alt="" src="images/autos/original/escalade2019.jpg">
							</div>
							<div class="work-post-content">
								<h5>Cadillac® Escalade 2019</h5>
								<span><a href="pdfs/escalade2019.pdf" target="_blank">Ficha técnica</a></span>
							</div>
						</div>   

						<div class="work-post suv">
							<div class="work-post-gal">
								<img alt="" src="images/autos/original/xt52018.jpg">
							</div>
							<div class="work-post-content">
								<h5>Cadillac® XT5 2019</h5>
								<span><a href="pdfs/xt52019.pdf" target="_blank">Ficha técnica</a></span>
							</div>
						</div>                      
						
							 
                        
						<div class="work-post auto">
							<div class="work-post-gal">
								<img alt="" src="images/autos/original/cts2019.jpg">
							</div>
							<div class="work-post-content">
								<h5>Cadillac® CTS 2019 </h5>
								<span><a href="pdfs/cts2019.pdf" target="_blank">Ficha técnica</a></span>
							</div>
						</div>
                        
						<div class="work-post suv">
							<div class="work-post-gal">
								<img alt="" src="images/autos/original/acadia2019.jpg">
							</div>
							<div class="work-post-content">
								<h5>GMC® Acadia 2019</h5>
								<span><a href="pdfs/acadia2019.pdf" target="_blank">Ficha técnica</a></span>
							</div>
						</div>
                        
						<div class="work-post pickup">
							<div class="work-post-gal">
								<img alt="" src="images/autos/original/sierra2018.jpg">
							</div>
							<div class="work-post-content">
								<h5>GMC® Sierra 2019 </h5>
								<span><a href="pdfs/sierra2018.pdf" target="_blank">Ficha técnica</a></span>
							</div>
						</div>
                        
						<div class="work-post pickup">
							<div class="work-post-gal">
								<img alt="" src="images/autos/original/sierraallterrain2018.jpg">
							</div>
							<div class="work-post-content">
								<h5>GMC® Sierra AT 2019</h5>
								<span><a href="pdfs/sierra2018.pdf" target="_blank">Ficha técnica</a></span>
							</div>
						</div>
                        
						<div class="work-post pickup">
							<div class="work-post-gal">
								<img alt="" src="images/autos/original/sierradenali2018.jpg">
							</div>
							<div class="work-post-content">
								<h5>GMC® Sierra Denali 2019</h5>
								<span><a href="pdfs/sierraallterrain2018.pdf" target="_blank">Ficha técnica</a></span>
							</div>
						</div>
                        
						<div class="work-post suv">
							<div class="work-post-gal">
								<img alt="" src="images/autos/original/terrain2019.jpg">
							</div>
							<div class="work-post-content">
								<h5>GMC® Terrain 2019</h5>
								<span><a href="pdfs/terrain2019.pdf" target="_blank">Ficha técnica</a></span>
							</div>
						</div>
                        
						<div class="work-post suv">
							<div class="work-post-gal">
								<img alt="" src="images/autos/original/yukondenali2019.jpg">
							</div>
							<div class="work-post-content">
								<h5>GMC® Yukon 2019</h5>
								<span><a href="pdfs/yukondenali2019.pdf" target="_blank">Ficha técnica</a></span>
							</div>
						</div>                        
                    
					</div>
				</div>
			</div>

		<!-- End content -->



		<!-- footer -->
		
<?php include('contenido/footer.php'); ?>
</body>
</html>