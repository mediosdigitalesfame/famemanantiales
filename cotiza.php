<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>Cotizador</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="FAME Manantiales, Morelia y Uruapan, Michoacán. Sitio WEB oficial FAME Manantiales Morelia y Uruapan. Te brindamos información sobre las mejores marcas Premium del mercado. Buick, GMC y Cadillac te invitan a conocer exclusiva gama de vehículos de lujo y todos los servicios que ofrecemos para tí. Piensa en auto, piensa en FAME. ">
    <meta name="keywords" content="FAME Manantiales Morelia, fame manantiales uruapan, buick morelia, buick michoacan, buick mexico, buick manantiales, agencia buick morelia, agencia buick uruapan, seminuevos buick morelia, usados buick morelia, usados buick uruapan, enclave morelia, enclave uruapan, enclave 2015 morelia, enclave 2015 uruapan, lacrosse morelia, buick lacrosse morelia, buick lacrosse uruapan, buick encore morelia, buick encore uruapan, encore morelia, encore uruapan, encore, lacrosse, enclave, verano, regal, verano morelia 2015, buick verano morelia, buick verano uruapan, buick verano 2015 mexico, regal morelia, regal uruapan, buick regal morelia, buick regal uruapan, buick regal seminuevo, buick regal usado, buick verano seminuevo, buick verano usado, buick lacrosse seminuevo, buick lacrosse usado, buick enclave seminuevo, buick enclave usado, buick encore nuevo, buick encore seminuevo, buick encore usado, buick encore demo, buick oferta, buick promociones, buick taller, buick lujo, buick 2015, buick equipamiento, buick caracteristicas, ofertas buick, buick seminuevo morelia, buick seminuevo uruapan, buick seminuevos, buick agencia morelia, buick agencia uruapan, concesionario buick, concesionario buick morelia, concesionario buick uruapan, buick morelia telefono, buick morelia ubicacion, buick morelia mapa, buick morelia fame, buick fame, buick grupo fame, buick manantiales, buick manantiales morelia, buick manantiales uruapan, agencia gmc morelia, agencia gmc uruapan, autos gmc morelia, coches gmc morelia, camioneta gmc morelia, camionetas gmc morelia, camionetas gmc usados, camionetas gmc nuevos, gmc lujo morelia, autos de lujo morelia, precio gmc, precio buick, gmc acadia, gmc acadia denali, gmc sierra, gmc sierra regular, gmc sierra crew cab, gmc sierra denali, gmc sierra 2015, gmc terrain, gmc terrain morelia, gmc terrain denali, gmc terrain usada, camioneta terrain morelia, compro terrain morelia, gmc yukon, camioneta yukon, gmc camioneta yukon, gmc yukon 2015, gmc yukon caracteristicas, gmc yukon nueva, gmc yukon usada, general motors morelia, general motors uruapan, general motors mapa general motors telefono, agencia general motors, cadillac morelia, cadillac uruapan, cadillac 2015 morelia, cadillac 2015 uruapan, cadillac seminuevos morelia, cadillac usados morelia, cadillac morelia precio, cadillac morelia precio, cadillac onstar, buick onstar, gmc onstar, cadillac shield, cadillac ats morelia, cadillac ats, cadillac ats 2015, cadillac ats coupe, cadillac ats sedan, cadillac ats uruapan, cadillac nuevo, cadillac ats nuevo, cadillac ats seminuevo, cadillac cts, cadillac cts morelia, cadillac cts precio, cadillac cts promocion, promociones cadillac, bonos cadillac, cadillac escalade nueva, cadillac escalade 2015, cadillac escalade apatzingan, cadillac escalade uruapan, cadillac escalade usada, cadillac escalade negra, cadillac escalade seminueva, cadillac srx, cadillac srx morelia, cadillac srx uruapan, grupo fame manantiales, grupo fame cadillac, grupo fame buick, grupo fame gmc, fame manantiales, fame cadillac, fame buick, fame gmc, fame manantiales uruapan, fame manantiales ubicacion, fame manantiales telefono, fame manantiales mapa, agencia fame manantiales, fame manantiales taller, fame manantiales servicio, servicio cadillac morelia, servicio buick morelia, servicio autos de lujo, servicio autos de lujo morelia, morelia, uruapan, michoacan, mexico">
    <meta name="author" content="Alejandro Cruz Saucedo">
    
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,700,600,300' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="css/bootstrap.css" type="text/css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/magnific-popup.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/owl.carousel.css" media="screen">
    <link rel="stylesheet" type="text/css" href="css/owl.theme.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/style.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/flexslider.css" media="screen">
	<link rel="stylesheet" type="text/css" href="css/responsive.css" media="screen">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <link rel="icon" type="image/png" href="/images/favicon.png" />

</head>
<body>

	<?php include('chat.php'); ?>

	<!-- Container -->
	<div id="container">
    
		<!-- Header -->
		<header class="clearfix">
			<!-- Static navbar -->
			<div class="navbar navbar-default navbar-fixed-top">
				<div class="top-line">
					<div class="container">
						<p>
							<span><i class="fa fa-phone"></i>Agencia: (443) 334 4440</span>
						</p>
						<ul class="social-icons">
							<li><a class="facebook" href="https://www.facebook.com/CadillacBuickYGmc?fref=ts" target="_blank"><i class="fa fa-facebook"></i></a></li>
							<li><a class="twitter" href="https://twitter.com/manantialesf" target="_blank"><i class="fa fa-twitter"></i></a></li>
							<li><a class="youtube" href="https://www.youtube.com/user/GrupoFameAutos" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
                            <li><a class="whatsapp" href="http://www.grupofame.com/whatsapp/" target="_blank"><i class="fa fa-whatsapp"></i></a></li>
                            <li><a class="instagram" href="https://www.instagram.com/grupofame" target="_blank"><i class="fa fa-instagram"></i></a></li>
                           	
						</ul>
					</div>
				</div>
                
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="index.php" target="_self"><img alt="Inicio" src="images/logo.png"></a>
					</div>
					<div class="navbar-collapse collapse">
						<ul class="nav navbar-nav navbar-right">

							<li><a class="active" href="index.php">Inicio</a></li>
							<li class="drop"><a href="autos.php">Vehículos</a>
								<ul class="drop-down">
									<li><a href="pdfs/enclave2018.pdf" target="_blank">Buick Enclave</a></li>
									<li><a href="pdfs/encore2018.pdf" target="_blank">Buick Encore</a></li>
									<li><a href="pdfs/envision2018.pdf" target="_blank">Buick Envision</a></li>
                                    <li><a href="pdfs/ats2018.pdf" target="_blank">Cadillac ATS</a></li>	
									<li><a href="pdfs/cts2018.pdf" target="_blank">Cadillac CTS</a></li>
									<li><a href="pdfs/escalade2018.pdf" target="_blank">Cadillac Escalade</a></li>
									<li><a href="pdfs/xt52018.pdf" target="_blank">Cadillac XT5</a></li>
									<li><a href="pdfs/acadia2018.pdf" target="_blank">GMC Acadia</a></li>	
									<li><a href="pdfs/sierradenali2018.pdf" target="_blank">GMC Sierra Denali</a></li>
                                    <li><a href="pdfs/sierraallterrain2018.pdf" target="_blank">GMC Sierra Crew Cab</a></li>
									<li><a href="pdfs/sierra2018.pdf" target="_blank">GMC Sierra</a></li>	
									<li><a href="pdfs/terrain2018.pdf" target="_blank">GMC Terrain Denali</a></li>
                                    <li><a href="pdfs/yukondenali2018.pdf" target="_blank">GMC Yukon</a></li>
                                </ul> </li>    

							<li class="drop"><a href="servicio.php">Servicio</a>
								<ul class="drop-down">
									<li><a href="servicio.php">Cita de Servicio</a></li>
                                    <li><a href="servicio-buick.php" target="_self">Servicio Buick</a></li>
                                    <li><a href="servicio-gmc.php" target="_self">Servicio GMC</a></li>
                                    <li><a href="servicio-cadillac.php" target="_self">Servicio Cadillac</a></li>
                                    <li><a href="refacciones.php">Refacciones</a></li>
                                    
                                  </ul> </li> 
                                  
							<li class="drop"><a href="#">Beneficios</a>
								<ul class="drop-down">
									<li><a href="garantia-buick.php">Garantía Buick</a></li>
                                    <li><a href="garantia-gmc.php" target="_self">Garantía GMC</a></li>
                                    <li><a href="garantia-cadillac.php" target="_self">Garantía Cadillac</a></li>
                                    <li><a href="onstar.php">On Star®</a></li>
                                    <li><a href="cadillac-shield.php">Cadillac Shield®</a></li>
                                  </ul> </li>                                   
                                  
                        
							<li class="drop"><a href="promociones.php">Promociones</a>
                            	<ul class="drop-down">
                                	<li><a href="promos-buick.php">Buick®</a></li>
                                	<li><a href="promos-gmc.php">GMC®</a></li>
                                	<li><a href="promos-cadillac.php">Cadillac®</a></li>
                                    <li><a href="gm-financial.php">GM® Financial</a></li>
                            	</ul></li>   
                                
                                                 
							<li class="drop"><a href="contacto.php">Contacto</a>
                            	<ul class="drop-down">
                                	<li><a href="contacto.php">Morelia</a></li>
                                    <li><a href="contacto-upn.php">Uruapan</a></li>
                                    <li><a href="manejo.php">Cita de Manejo</a></li>
                                    <li><a href="cotiza.php">Cotizador de auto</a></li>
                                  
                                </ul></li>
							<li><a href="ubicacion.php">Ubicación</a></li>
                        </ul>
                        
					</div>
				</div>
			</div>
		</header>
		<!-- End Header -->
        
<!-- ANALYTICS MANANTIALES-->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-47755725-1', 'auto');
  ga('send', 'pageview');

</script>

<!-- FIN ANALYTICS -->   

		<!-- content 
			================================================== -->
		<div id="content">

			<!-- Page Banner -->
			<div class="page-banner">         

				<div class="container">
					<h2>Cotiza tu Auto</h2>
				</div>
			</div>

			<div class="about-box">
				<div class="container">
                
							<div class="single-project-content">
								<img alt="" src="images/manejo.jpg">
                         	</div>                  

						<div class="col-md-12" align="center">
							<h3>Cotiza tu próximo vehículo</h3>
<?php
	if (isset($_POST['boton'])) {
        if($_POST['nombre'] == '') {
        	$errors[1] = '<span class="error">Ingrese su nombre</span>';
        } else if($_POST['email'] == '' or !preg_match("/^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/",$_POST['email'])){
        	$errors[2] = '<span class="error">Ingrese un email correcto</span>';
        } else if($_POST['telefono'] == '') {
        	$errors[3] = '<span class="error">Ingrese un teléfono</span>';
        } else if($_POST['mensaje'] == '') {
        	$errors[4] = '<span class="error">Ingrese un mensaje</span>';
        } else {
        	$dest = "gerencia@famemanantiales.com, ventas@famemanantiales.com" . ','. "formas@grupofame.com";
            $nombre = $_POST['nombre'];
            $email = $_POST['email'];
            $telefono = $_POST['telefono'];
			$asunto_cte = "Cotizador Auto FAME Manantiales";
			$asunto = "Cotizador Auto FAME Manantiales";
            $cuerpo = $_POST['mensaje'];
			$cuerpo_mensaje .= $nombre . "<br>" . "Mensaje: ". $cuerpo . "<br>" . "Mi correo es: " . $_POST['email'] . "<br>" . "Mi teléfono es: " . $_POST['telefono'];
			$cuerpo_cte = '
			<html>
			<head>
			  <title>Mail from '. $nombre .'</title>
			</head>
			<body>
			  <table style="width: 500px; font-family: arial; font-size: 14px;" border="1">
				<tr style="height: 32px;">
				  <th align="right" style="width:150px; padding-right:5px;">Nombre:</th>
				  <td align="left" style="padding-left:5px; line-height: 20px;">'. $nombre .'</td>
				</tr>
				<tr style="height: 32px;">
				  <th align="right" style="width:150px; padding-right:5px;">Correo:</th>
				  <td align="left" style="padding-left:5px; line-height: 20px;">'. $email .'</td>
				</tr>
				<tr style="height: 32px;">
				  <th align="right" style="width:150px; padding-right:5px;">Mensaje:</th>
				  <td align="left" style="padding-left:5px; line-height: 20px;">'. $cuerpo .'</td>
				</tr>
			  </table>
			</body>
			</html>
			';
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
			$headers .= 'From: ' . $email . "\r\n";
            
            if(mail($dest,$asunto,$cuerpo_mensaje,$headers)){
            	$result = '<div class="result_ok">Email enviado correctamente, en breve nos comunicaremos contigo. Gracias por tu preferencia. "Piensa en Auto, Piensa en FAME"</div>';
				mail($email,$asunto_cte,$cuerpo_cte,$headers);
                $_POST['nombre'] = '';
                $_POST['email'] = '';
                $_POST['telefono'] = '';
                $_POST['mensaje'] = '';
            } else {
            	$result = '<div class="result_fail">Hubo un error al enviar el mensaje</div>';
            }
        }
    }
?>
<html>
	<meta charset="utf-8">
    <link rel='stylesheet'| href='css/formularios.css'>
    <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js'></script>
	<form id="contact-form" class="contact-work-form2" method='post' action=''>
    	<div class="text-input">
        	<div class="float-input">
            <input name='nombre' id="nombre" placeholder="Nombre*" type='text' class='nombre' value='<?php if(isset($_POST['nombre'])){ echo $_POST['nombre']; } ?>'> 
        <?php if(isset($errors)){ echo $errors[1]; } ?><span><i class="fa fa-user"></i></span></div>
        
        <div class="float-input2"><input name='email' placeholder="E-mail*" type='text' class='email' value='<?php if(isset($_POST['email'])){ echo $_POST['email']; } ?>'>
        <?php if(isset($errors)){ echo $errors[2]; } ?><span><i class="fa fa-envelope"></i></span></div>
        </div>
        
        <div class="text-input">
        <div class="float-input">
        <input name='telefono' id="telefono" placeholder="Teléfono*" type='tel' class='telefono' value='<?php if(isset($_POST['telefono'])){ echo $_POST['telefono']; } ?>'>
        <?php if(isset($errors)){ echo $errors[3]; } ?><span><i class="fa fa-phone"></i></span>
        </div>
        </div>
        
        <div class="textarea-input"><textarea name='mensaje' placeholder="Mensaje*" rows='5' class='mensaje'><?php if(isset($_POST['mensaje'])){ echo $_POST['mensaje']; } ?></textarea>
        <?php if(isset($errors)){ echo $errors[4]; } ?><span><i class="fa fa-comment"></i></span>
        </div>
        <div><input name='boton' type='submit' value='Enviar' class='boton'></div>
        <br><br><br>

<?php if(isset($result)) { echo $result; } ?>
    </form><br><br><br>

						</div>
                
					<div class="row">
				</div>
			</div>



<!--barra información limpia-->
		<div class="section">
			<div id="about-section">

               </div>
             </div>
 



		<!-- footer -->
		<footer>
			<div class="footer-line">
				<div class="container">
					<p><span><i class="fa fa-phone"></i> 01800 670 8386 | </span> 2018 Fame Manantiales | <i class="fa fa-user"> </i><a href="aviso.php" target="_self"><font color="#FFFFFF"><strong>  Aviso de privacidad</strong></font></a></p>
					<a class="go-top" href="#"></a>
				</div>
			</div>

		</footer>
		<!-- End footer -->
        
	</div>
	<!-- End Container -->

	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.migrate.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script type="text/javascript" src="js/retina-1.1.0.min.js"></script>
	<script type="text/javascript" src="js/plugins-scroll.js"></script>
	<script type="text/javascript" src="js/script.js"></script>

	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

</body>
</html>