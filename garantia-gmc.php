<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>Garantía GMC®</title>
	<?php include('contenido/head.php'); ?>
</head>
<body>

	<?php include('chat.php'); ?>

	<!-- Container -->
	<div id="container">
    
		<?php include('contenido/header.php'); ?>
		<?php include('contenido/analytics.php'); ?>
		<div id="content">

			<!-- Page Banner -->
			<div class="page-banner">         

				<div class="container">
					<h2>Garantía GMC® </h2>
				</div>
			</div>

			<div class="about-box">
				<div class="container">
					<div class="row">
				</div>
			</div>



<!--barra información limpia-->
		<div class="section">
			<div id="about-section">

				<div class="welcome-box">
					<div class="container">
						<p align="justify">La compra de un auto es una inversión importante, por lo que a la hora de elegir, la decisión se torna difícil, y usted al adquirir su nuevo vehículo General Motors ha acertado plenamente, <strong>¡eso se lo garantizamos!</strong>
</p><br><br>

<p align="justify">
Le recordamos que su vehículo GMC® cuenta con una garantía que planta ofrece a todos sus clientes de 2 años o 60,000 Kilómetros (lo que ocurra primero). Y para que nuestros clientes puedan prolongar su tranquilidad, ponemos a su disposición el plan de Extensión de Garantía, “Garantía GM Plus®”, el cual cuenta con 3 diferentes opciones que usted puede adquirir, según la que más se adapte a sus necesidades:
<br><br>

							<div class="single-project-content">
								<img alt="" src="images/tabla-garantia-GMC.jpg">
                         	</div>

<p align="justify">
En todas las opciones, la garantía se dará por vencida al cumplir cualquiera de los dos rubros (tiempo o kilometraje) y puede ser adquirida siempre y cuando el vehículo tenga menos de 20 meses de comprado como nuevo y 30,000 km.<br><br>

No obstante a la excelente calidad de los vehículos General Motors, éstos se encuentran expuestos a desperfectos impredecibles que pueden ser ocasionados por el desgaste del propio uso de su vehículo, por lo que contar con la “Garantía GM Plus®” le permitirá obtener los beneficios que la misma otorga, de acuerdo al plan elegido por usted.
</p><br><br>


<p align="left"><strong>Cobertura</strong></p><br>   
<p align="justify">
La Garantía GM Plus®, le permitirá disfrutar de una cobertura similar a la garantía "Defensa a Defensa" de los primeros 2 años o 40.000 km. de su vehículo General Motors, por un periodo que puede llegar a ser hasta de 3 años más a partir del vencimiento de la garantía original o hasta 60,000 km adicionales (lo que ocurra primero).
</p><br>

<div class="col-md-6">
<p align="justify">
• Motor<br>
• Transmisión<br>
• Transeje (Diferencial) Trasero o Delantero<br>
• Sistema eléctrico<br><br><br>
</p>
</div>

<div class="col-md-6">
<p align="justify">
• Sistema de enfriamiento<br>
• Sistema de frenos<br>
• Dirección<br>
• Suspensión<br>
</p><br><br><br>

</div>

<p align="justify">*Esto es un ejemplo ilustrativo, para mayor información de la cobertura, acudir a su Distribuidor Autorizado GM®.</p><br><br>

<p align="left"><strong>Incluye Asistencia en el Camino GM®</strong></p><br>   
<p align="justify">
La Garantía GM Plus® en todas las opciones, le extiende los beneficios de Asistencia en el Camino GM® por el periodo de vigencia de su Garantía GM Plus®. Lo cual le brinda protección y seguridad adicionales a lo que la garantía le ofrece. Esto le puede significar ahorros importantes.
</p><br><br>

<p align="left"><strong>¿Cómo puedo adquirir una Garantía GM Plus®?</strong></p><br>   
<p align="justify">
Usted puede contratar su GM Garantía Plus® a través del distribuidor de su preferencia. Esto le brinda la seguridad de que estará cubierto contra gastos inesperados. Para mayor información del Programa de Garantía GM Plus®, acuda ante su Distribuidor Autorizado GM®.
</p><br><br>

<p align="left"><strong>Tiempos de venta</strong></p><br>   
<p align="justify">
Podrá adquirir su garantía extendida en dos momentos:<br>
<strong>1°- </strong>A partir de la fecha de factura y hasta los 9 meses o 10 mil km lo que ocurra primero.<br>
<strong>2°- </strong>Después de 9 meses o 10 mil km y hasta 20 meses o 30 mil km. 
</p><br><br>
</p></div></div>
                
                
<?php include('contenido/footer.php'); ?>
     </div> 			
	
</body>
</html>