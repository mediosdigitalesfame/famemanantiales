<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>Cadillac Shield®</title>
	<?php include('contenido/head.php'); ?>
</head>
<body>

	<?php include('chat.php'); ?>

	<!-- Container -->
	<div id="container">
    
		<?php include('contenido/header.php'); ?>
		<?php include('contenido/analytics.php'); ?>
		
			<div class="page-banner">
				<div class="container">
					<h2>Cadillac® Shield</h2>

				</div>
			</div>


			<!-- contact box -->

				<div class="welcome-box">
					<div class="container">
                    
							<div class="single-project-content">
								<img alt="" src="images/cadillac-shield.jpg">
                         	</div>                    
						
<!--<iframe width='100%' height='500px' frameborder="0" allowfullscreen src='https://www.youtube.com/embed/EywAb4QcaV8?rel=0&autoplay=1'></iframe>  <br>
<br><br>-->
                                   <h3><strong>Cadillac® Shield</strong></h3>
<p align="justify">Es el nuevo programa de servicios exclusivos para los propietarios Cadillac. Con él obtienes, además de un vehículo de imponente diseño, una serie de beneficios adicionales que otorgan el trato especial que mereces.</p>    <br><br><br>                               

				<!-- ENLACES -->
				<div class="services-box">
					<div class="container">
						<div class="row">
							<div class="col-md-4">
								<div class="services-post">
									<div class="services-post-content">
										<h4><a><strong>OnStar®</strong></a></h4>
										<p align="center">En cualquier momento presione el botón azul de OnStar® y será conectado en vivo a un Asesor OnStar®. Sin grabaciones automáticas, sólo personal capacitado listo para ayudarlo 24 horas al día, 7 días a la semana y 365 días al año. 
                                        <i>Servicio exclusivo de OnStar®</i></p>
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="services-post">
									<div class="services-post-content">
										<h4><a><strong>Mantenimiento Cadillac</strong></a></h4>
										<p align="center">En todos los modelos Cadillac a partir del 2012, el Mantenimiento Cadillac está incluido. Durante 3 años o 60,000 kilómetros de acuerdo a las operaciones e intervalos establecidos en la Póliza de Garantía.</p>
									</div>
								</div>
							</div>
                            
							<div class="col-md-4">
								<div class="services-post">
									<div class="services-post-content">
										<h4><a><strong>SOS Emergencia</strong></a></h4>
										<p align="center">Asistencia en caso de accidentes o ante cualquier emergencia, incluso si usted no puede solicitarla. <i>Servicio exclusivo de OnStar®</i></p><br><br><br><br>
<br><br>
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="services-post">
									<div class="services-post-content">
										<h4><a><strong>Seguridad</strong></a></h4>
										<p align="center">Servicio remoto de apertura y cerrado de puertas con una sola llamada a nuestro Call Center. Asistencia a las autoridades en la localización del vehículo vía GPS en caso de robo. <i>Servicio exclusivo de OnStar®</i></p>
									</div>
								</div>
							</div>
                            
							<div class="col-md-4">
								<div class="services-post">
									<div class="services-post-content">
										<h4><a><strong>Diagnóstico</strong></a></h4>
										<p align="center">Diagnóstico mensual del desempeño de su vehículo.
Revisión remota de sistemas de funcionamiento del vehículo mientras maneja. <i>Servicio exclusivo de OnStar®</i></p><br><br><br>
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="services-post">
									<div class="services-post-content">
										<h4><a><strong>Navegación</strong></a></h4>
										<p align="center">Asesoramiento con direcciones paso a paso que le guiarán hasta su destino incluso si se desvía de la ruta original.<i>Servicio exclusivo de OnStar®</i></p><br><br><br>
									</div>
								</div>
							</div>   
                            
                            
							<div class="col-md-4">
								<div class="services-post">
									<div class="services-post-content">
										<h4><a><strong>Conectividad</strong></a></h4>
										<p align="center">Conéctate a tu vehículo de manera remota con la aplicación móvil <strong>RemoteLink™</strong> y controla tu vehículo desde la palma de tu mano, abre o cierra las puertas, enciende el motor y accede a la información de tu vehículo.<i>Servicio exclusivo de OnStar®</i></p>
									</div>
								</div>
							</div>
                            
							<div class="col-md-4">
								<div class="services-post">
									<div class="services-post-content">
										<h4><a><strong>Asistencia en el Camino</strong></a></h4>
										<p align="center">Este programa es independiente de la garantía que tiene tu vehículo; es la asistencia que tiene el conductor y sus pasajeros durante la conducción de su vehículo dentro de su ciudad de residencia o en alguna carretera transitable dentro de México, Estados Unidos y Canadá.</p>
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="services-post">
									<div class="services-post-content">
										<h4><a><strong>Cadillac Concierne</strong></a></h4>
										<p align="center">Los propietarios Cadillac son acreedores al servicio Cadillac Concierge, mismo que además de contar con asistencia en eventualidades como avería y robo del vehículo dentro de México, Estados Unidos y Canadá, ofrece servicios exclusivos.</p>
									</div>
								</div>
							</div>                                                                                  
                        

						</div>
					</div>

                                 
				<img class="shadow-image" alt="" src="images/shadow.png">
				</div>
                
               

			</div>



		</div>
		<!-- FIN ENLACES -->

   

                	</div>
				</div>

		</div>
		<!-- End content -->


<?php include('contenido/footer.php'); ?>
     </div> 			
	
</body>
</html>