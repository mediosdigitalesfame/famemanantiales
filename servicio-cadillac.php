<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>Servicio Cadillac®</title>
	<?php include('contenido/head.php'); ?>
</head>
<body>

	<?php include('chat.php'); ?>

	<!-- Container -->
	<div id="container">
    	<?php include('contenido/header.php'); ?>
        <?php include('contenido/analytics.php'); ?>
			<!-- Page Banner -->
			<div class="page-banner">
				<div class="container">
					<h2>Servicio Cadillac® FAME Manantiales</h2>

				</div>
			</div>


			<!-- contact box -->

				<div class="welcome-box">
					<div class="container">
                    
            
							<div class="single-project-content">
								<img alt="" src="images/serv-basico-cadillac2015.jpg">
                         	</div>                    
						
						<p align="justify">A partir de los modelos 2014 la frecuencia de servicio básico es cada 10,000 km o 12 meses, lo que ocurra primero. Los servicios arriba indicados incluyen aceite de motor SINTÉTICO DEXOS, refacciones originales y mano de obra de acuerdo con el programa de mantenimiento de la póliza del vehículo. Todos los precios incluyen IVA. Válidos en la República Mexicana del 01 de enero al 30 de junio de 2016. Precios sugeridos por General Motors de México, S. de R.L. de C.V. D.R., © General Motors de México, S. de R.L. de C.V., Av. Ejército Nacional 843, Col. Granada, 11520, México D.F. 2016</p><br><br><br><br>
 
 
           
							<div class="single-project-content">
								<img alt="" src="images/serv-basico-cadillac2013.jpg">
                         	</div>                    
						
						<p align="justify">Los servicios arriba indicados incluyen aceite de motor SINTÉTICO DEXOS, refacciones originales y mano de obra de acuerdo con el programa de mantenimiento de la póliza del vehículo. Todos los precios incluyen IVA. Válidos en la República Mexicana del 01 de enero al 30 de junio de 2016. Precios sugeridos por General Motors de México, S. de R.L. de C.V. D.R., © General Motors de México, S. de R.L. de C.V., Av. Ejército Nacional 843, Col. Granada, 11520, México D.F. 2016</p><br><br><br><br>
                        
                        
           
							<div class="single-project-content">
								<img alt="" src="images/serv-frenos-cadillac.jpg">
                         	</div>                    
						
						<p align="justify">Precios sugeridos por General Motors de México S. de R.L de C.V. Incluyen balatas delanteras y traseras originales, instalación, rotación de ruedas e I.V.A. No incluye rectificado de discos, en caso de requerirlo se considera como una operación adicional. Para información de otros modelos pregunte al Consultor de Servicio en su Distribuidor Autorizado Buick-GMC. Precios válidos en la República Mexicana del 01 de enero al 30 de junio de 2016. D.R. © General Motors de México S. de R.L de C.V. , Av. Ejército Nacional 843, Col. Granada, 11520, México D.F. 2016.</p><br><br><br><br>

                	</div>
				</div>

		</div>
		<!-- End content -->


<?php include('contenido/footer.php'); ?>

</body>
</html>