<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>Refacciones</title>
	<?php include('contenido/head.php'); ?>
</head>
<body>

	<?php include('chat.php'); ?>

	<!-- Container -->
	<div id="container">
    	<?php include('contenido/header.php'); ?>
        <?php include('contenido/analytics.php'); ?>
		<div id="content">

			<!-- Page Banner -->
			<div class="page-banner">         

				<div class="container">
					<h2>Refacciones y Accesorios Morelia</h2>
				</div>
			</div>

			<div class="about-box">
				<div class="container">
                
							<div class="single-project-content">
								<img alt="" src="images/refacciones2.jpg">
                         	</div>                

								<div class="col-md-3">
							 
						</div>

                       <div class="col-md-6" align="center">
                                 <div class="container">
					                 <div class="col-md-12" >
								         <?php include('form.php'); ?>
                                     </div>
                                 </div>
                            </div>
		<div class="section">
			<div id="about-section">




				<div class="welcome-box">
					<div class="container">
                    	<div class="col-md-6">
                
						<h1><span>Refacciones y Accesorios GM</span></h1><br>
						<p align="justify"><strong>Tu vehículo, tan único como tú</strong><br><br>

Haz que tu auto se distinga dentro de un estacionamiento gracias al estilo y personalidad que le imprimes con los diversos accesorios que se te brindan.<br><br>

Entre la línea más completa de accesorios, podrás encontrar: alerones, llantas, antenas de techo, equipos de sonido, estribos, bocinas, faldones laterales, detalles cromados, entre otros. Asimismo, cada uno incluye una garantía única que sólo GM puede ofrecer.</p><br><br>
   
<p align="justify"><strong>Conveniencia</strong></p><br>
<p align="justify">Si lo deseas, puedes adquirir e instalar tus Accesorios GM originales en una sola ocasión, con la comodidad de incluir el costo en el mismo contrato de financiamiento de tu vehículo y la tranquilidad de contar con la garantía GM.<br>
</p><br><br>
						</div>
                        <div class="col-md-6"><br><br><br><br><br>
<p align="justify"><strong>Calidad</strong></p><br>
<p align="justify">Los Accesorios GM originales te dan la seguridad de obtener la más alta calidad en materiales, fabricación y durabilidad; ya que han sido diseñados conjuntamente con el desarrollo de tu vehículo.<br>
</p><br><br>

<p align="justify"><strong>Tanquilidad</strong></p><br>
<p align="justify">Al instalar Accesorios GM originales no se afecta el comportamiento, seguridad e integridad de tu automóvil, porque GM respalda sus productos.<br>
</p><br><br>

<p align="justify"><strong>Estilo</strong></p><br>
<p align="justify">Diseños innovadores, alta tecnología y rigurosos estándares de fabricación. Todo esto y más se conjuga en la amplia línea de Accesorios GM originales.<br>

Para mayor información, acude a cualquiera de nuestras distribuidoras.<br>
</p>


					</div>
<br>


                	</div>
				</div>
 
               </div>
             </div>
                </div>
            </div>
        </div>
 <?php include('contenido/footer.php'); ?>
     </div> 			
	
</body>
</html>