<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>AUTOS</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Site Description Here">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/stack-interface.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/socicon.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/flickity.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/iconsmind.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/jquery.steps.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/theme.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/font-frankruhl-firasans.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Fira+Sans:400,400i,500,700%7CFrank+Ruhl+Libre:300,400" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

    </head>
    <body class=" " data-smooth-scroll-offset='64'>
        <a id="start"></a>
        <?php include('menu.php'); ?>  
          

             <br>
<section class=" bg--secondary">
                <div class="container">

                    <div class="row justify-content-center">

                        <div class="col-md-4 col-lg-4">
                        </div>

                        <div class="col-md-6 col-lg-6">
                            <div class="row">
                                <div style="font-size: 28px; " >

                                    <button href="https://api.whatsapp.com/send?phone=524433250949&text=Hola,%20Quiero%20más%20información!" class="btn btn-success">
                                        
                                    &nbsp; <i style="font-size: 32px; color:white;" class="fab fa-whatsapp"></i>   WhatsApp &nbsp; <br>
                                                
                                    </button>
                                </div>
                            </div>
                            <!--end of row-->
                        </div>

                    </div>

                    <br>


                    <div class="row justify-content-center">

                        

                        <div class="col-md-6 col-lg-6">
                            <div class="row">
                                <div class="boxed boxed--border">
                                    <form class="text-left form-email row" data-success="Gracias por contactarnos, en breve un asesor te contactará." data-error="Por favor ingresa los datos correctos." >
                                        <div class="col-md-6">
                                            <span>Nombre:</span>
                                            <input type="text" name="name" class="validate-required" />
                                        </div>
                                        <div class="col-md-6">
                                            <span>Ciudad:</span>
                                            <input type="text" name="company" class="validate-required" />
                                        </div>
                                        <div class="col-md-6">
                                            <span>Correo:</span>
                                            <input type="email" name="email" class="validate-required validate-email" />
                                        </div>
                                        <div class="col-md-6">
                                            <span>Telefono:</span>
                                            <input type="tel" name="phone" class="validate-required" />
                                        </div>
                                        <div class="col-md-12">
                                            <span>Comentarios:</span>
                                            <textarea rows="3" name="description" class="validate-required"></textarea>
                                        </div>
                                        <div class="col-md-12 text-center boxed">
                                            <h5>Servicios requeridos</h5>
                                        </div>
                                        <div class="col-md-3 col-6 text-center">
                                            <span class="block">1</span>
                                            <div class="input-checkbox">
                                                <input type="checkbox" name="design" />
                                                <label></label>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-6 text-center">
                                            <span class="block">2</span>
                                            <div class="input-checkbox">
                                                <input type="checkbox" name="dev" />
                                                <label></label>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-6 text-center">
                                            <span class="block">3</span>
                                            <div class="input-checkbox">
                                                <input type="checkbox" name="brand" />
                                                <label></label>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-6 text-center">
                                            <span class="block">4</span>
                                            <div class="input-checkbox">
                                                <input type="checkbox" name="marketing" />
                                                <label></label>
                                            </div>
                                        </div>
                                         
                                       
                                         
                                        <div class="col-md-12 boxed">
                                            <button type="submit" class="btn btn--primary type--uppercase">Enviar</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!--end of row-->
                        </div>

                        <div class="col-md-1 col-lg-1">
                        </div>


                        <div class="col-md-5 col-lg-5">
                            <div class="row">
                                <div class="boxed boxed--border ">
                                    <form class="text-left form-email row" data-recaptcha-theme="light">
                                        <div class="col-md-12">
                                            

                                            <h3>Información de Contacto</h3>

                             

                            <p>
                                <span class="block"><strong><i class="fa fa-map-marker"></i> Dirección:</strong> Periférico Paseo de la República #2655 Col. Las Camelinas  Morelia, Michoacán.</span>
                                <span class="block"><strong><i class="fa fa-phone"></i> Tel:</strong> <a href="tel:(443) 334 4440">(443) 334 4440</a></span>
                                <span class="block"><strong><i class="fa fa-envelope"></i> Email:</strong> <a href="mailto:mail@yourdomain.com">contacto@famemanantiales.com</a></span>
                            </p>

                            <hr />

                            <p>

                                <span class="block"><strong><i class="fa fa-phone"></i> Recepción:</strong> <a href="tel:(443) 334 4440">Ext. 0</a></span>
                                <span class="block"><strong><i class="fa fa-phone"></i> Taller de Servicio:</strong> <a href="tel:(443) 334 4440">Ext. 100</a></span>
                                <span class="block"><strong><i class="fa fa-phone"></i> Postventa:</strong> <a href="tel:(443) 334 4440">Ext. 101</a></span>
                                <span class="block"><strong><i class="fa fa-phone"></i> Refacciones:</strong> <a href="tel:(443) 334 4440">Ext. 300</a></span>
                                <span class="block"><strong><i class="fa fa-phone"></i> Ventas:</strong> <a href="tel:(443) 334 4440">Ext. 200</a></span>
                                <span class="block"><strong><i class="fa fa-phone"></i> Seminuevos:</strong> <a href="tel:(443) 334 4440">Ext. 203</a></span>
                                <span class="block"><strong><i class="fa fa-phone"></i> Financiamiento:</strong> <a href="tel:(443) 334 4440">Ext. 204</a></span>
                                
                            </p>

                                <hr />

                            <h3>WhatsApp</h3>

                        

                            <p>
                                <span class="block"><strong><i class="fa fa-phone"></i> </strong> <a href="tel:4432279923">4432279923</a></span>
                                 
                                
                            </p>

                                            <h3>Horario de Atención</h3>

                            <p>
                                Con gusto esperamos tu llamada en nuestro Call Center, para cualquier duda, aclaración o sugerencia que quieras compartirnos en FAME Manantiales; te escuchamos y atendemos de manera personalizada.
                            </p>

                            <hr />
 

                            <h4 class="font300">Ventas</h4>
                            <p>
                                <span class="block"><strong>Lunes - Viernes:</strong> 09:00 - 19:30 hrs.</span>
                                <span class="block"><strong>Sábado:</strong> 09:00 - 17:00 hrs.</span>
                                <span class="block"><strong>Domingo:</strong> 11:00 - 15:00 hrs.</span>
                            </p>

                            <h4 class="font300">Servicio y Administración</h4>
                            <p>
                                <span class="block"><strong>Lunes - Viernes:</strong> 09:00 - 19:30 hrs.</span>
                                <span class="block"><strong>Sábado:</strong> 09:00 - 14:00 hrs.</span>
                                <span class="block"><strong>Domingo:</strong> Cerrado</span>
                            </p>


                                        </div>
                                         
                                        
                                         
                                    </form>
                                </div>
                            </div>
                            <!--end of row-->
                        </div>
 

                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>
 
           
           <br>

                
               <div class="section">     
            <div id="about-section"> 

                <div class="welcome-box">
                    <div class="container">
                       <!-- <div class="col-md-6"> -->
                
                        <text align="center"><h1><span>Refacciones y Accesorios GM</span></h1><br></text>
                        <p align="justify"><strong>Tu vehículo, tan único como tú</strong><br><br>

                Haz que tu auto se distinga dentro de un estacionamiento gracias al estilo y personalidad que le imprimes con los diversos accesorios que se te brindan.

                Entre la línea más completa de accesorios, podrás encontrar: alerones, llantas, antenas de techo, equipos de sonido, estribos, bocinas, faldones laterales, detalles cromados, entre otros. Asimismo, cada uno incluye una garantía única que sólo GM puede ofrecer.</p>
                   
                <p align="justify"><strong>Conveniencia</strong></p>
                <p align="justify">Si lo deseas, puedes adquirir e instalar tus Accesorios GM originales en una sola ocasión, con la comodidad de incluir el costo en el mismo contrato de financiamiento de tu vehículo y la tranquilidad de contar con la garantía GM.<br>
                </p>
                    </div>

                    <div class="container"><br>

                <p align="justify"><strong>Calidad</strong></p>
                <p align="justify">Los Accesorios GM originales te dan la seguridad de obtener la más alta calidad en materiales, fabricación y durabilidad; ya que han sido diseñados conjuntamente con el desarrollo de tu vehículo.<br>
                </p>

                <p align="justify"><strong>Tanquilidad</strong></p>
                <p align="justify">Al instalar Accesorios GM originales no se afecta el comportamiento, seguridad e integridad de tu automóvil, porque GM respalda sus productos.<br>
                </p>

                <p align="justify"><strong>Estilo</strong></p>
                <p align="justify">Diseños innovadores, alta tecnología y rigurosos estándares de fabricación. Todo esto y más se conjuga en la amplia línea de Accesorios GM originales.<br>

                Para mayor información, acude a cualquiera de nuestras distribuidoras.<br>
                </p>


                    </div>
<br>


                    </div>
                </div>
 
               </div>
             </div>
                </div>
            </div>
        <!--</div> -->
           <br>
 
           
           
           </div>
             
           
            <div class="modal-container">
                <div class="modal-content">
                    <section class="imageblock feature-large bg--white border--round ">
                        <div class="imageblock__content col-lg-5 col-md-3 pos-left">
                            <div class="background-image-holder">
                                <img alt="image" src="img/cowork-8.jpg" />
                            </div>
                        </div>
                        <div class="container">
                            <div class="row justify-content-end">
                                <div class="col-lg-6 col-md-7">
                                    <div class="row">
                                        <div class="col-md-11 col-lg-10">
                                            <h1>Ideal for design conscious startups.</h1>
                                            <p class="lead">
                                                Start building a beautiful site for your startup &mdash; right in the comfort of your browser.
                                            </p>
                                            <hr class="short">
                                            <form>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <input type="email" name="Email Address" placeholder="Email Address" />
                                                    </div>
                                                    <div class="col-12">
                                                        <input type="password" name="Password" placeholder="Password" />
                                                    </div>
                                                    <div class="col-12">
                                                        <button type="submit" class="btn btn--primary type--uppercase">Create Account</button>
                                                    </div>
                                                    <div class="col-12">
                                                        <span class="type--fine-print">By signing up, you agree to the
                                                            <a href="#">Terms of Service</a>
                                                        </span>
                                                    </div>
                                                </div>
                                                <!--end row-->
                                            </form>
                                        </div>
                                        <!--end of col-->
                                    </div>
                                    <!--end of row-->
                                </div>
                            </div>
                            <!--end of row-->
                        </div>
                        <!--end of container-->
                    </section>
                </div>
            </div>
            <footer class="footer-3 text-center-xs space--xs bg--dark ">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <img alt="Image" class="logo" src="img/logo-dark.png" />

                             
                            <ul class="list-inline list--hover">
                                <li class="list-inline-item">
                                    <a href="#">
                                        <span class="type--fine-print"><i class="fa fa-phone"> </i> 01 800 670 8386 </span>
                                    </a>
                                </li>
                                 
                            </ul>
                        </div>

                         <div class="col-md-5">
                            <span class="type--fine-print">&reg;
                                <span class="update-year"></span> FAME Manantiales.</span>
                            <a class="type--fine-print" href="#">Aviso de Privasidad </a>
                            <a class="type--fine-print" href="#">Formato ARCO</a>
                        </div>

                        <div class="col-md-3 text-right text-center-xs">
                            <ul class="social-list list-inline list--hover">
                                <li class="list-inline-item">
                                    <a href="#">
                                        <i class="socicon socicon-google icon icon--xs"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#">
                                        <i class="socicon socicon-twitter icon icon--xs"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#">
                                        <i class="socicon socicon-facebook icon icon--xs"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#">
                                        <i class="socicon socicon-instagram icon icon--xs"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <!--end of row-->
                    
                    
                </div>
                <!--end of container-->
            </footer>
        </div>
        <!--<div class="loader"></div>-->
        <a class="back-to-top inner-link" href="#start" data-scroll-class="100vh:active">
            <i class="stack-interface stack-up-open-big"></i>
        </a>
        <script src="js/jquery-3.1.1.min.js"></script>
        <script src="js/flickity.min.js"></script>
        <script src="js/easypiechart.min.js"></script>
        <script src="js/parallax.js"></script>
        <script src="js/typed.min.js"></script>
        <script src="js/datepicker.js"></script>
        <script src="js/isotope.min.js"></script>
        <script src="js/ytplayer.min.js"></script>
        <script src="js/lightbox.min.js"></script>
        <script src="js/granim.min.js"></script>
        <script src="js/jquery.steps.min.js"></script>
        <script src="js/countdown.min.js"></script>
        <script src="js/twitterfetcher.min.js"></script>
        <script src="js/spectragram.min.js"></script>
        <script src="js/smooth-scroll.min.js"></script>
        <script src="js/scripts.js"></script>
    </body>
</html>