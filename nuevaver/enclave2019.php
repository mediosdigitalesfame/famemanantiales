<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>AUTOS</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Site Description Here">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/stack-interface.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/socicon.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/flickity.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/iconsmind.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/jquery.steps.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/theme.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/font-frankruhl-firasans.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Fira+Sans:400,400i,500,700%7CFrank+Ruhl+Libre:300,400" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

    </head>
    <body class=" " data-smooth-scroll-offset='64'>
        <a id="start"></a>
       

                <?php include('menu.php'); ?>

        <div class="main-container">

 
            <section class="text-center">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-10 col-lg-8">
                            <h2>Obtener mas información.</h2>
                            
                            <form class="row justify-content-center" action="//mrare.us8.list-manage.com/subscribe/post?u=77142ece814d3cff52058a51f&amp;id=f300c9cce8" data-success="Thanks for signing up.  Please check your inbox for a confirmation email." data-error="Please provide your name and email address and agree to the terms.">
                                <div class="col-md-4">
                                    <input class="validate-required" type="text" name="NAME" placeholder="Tu Nombre" />
                                </div>
                                <div class="col-md-4">
                                    <input class="validate-required validate-email" type="email" name="EMAIL" placeholder="Correo Electrónico" />
                                </div>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn--primary type--uppercase">Obtener Información</button>
                                </div>
                                <div class="col-md-12">
                                    <input class="validate-required" type="checkbox" name="group[13737][1]" />
                                    <span>Acepto
                                        <a href="#">terminos y condiciones</a>
                                    </span>
                                </div>
                                <div style="position: absolute; left: -5000px;" aria-hidden="true">
                                    <input type="text" name="b_77142ece814d3cff52058a51f_f300c9cce8" tabindex="-1" value="">
                                </div>
                            </form>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>

            <section class="switchable">
                <div class="container">
                    <div class="row justify-content-around">
 
                        <div class="col-md-12 text-center">

                            <h1>Buick Enclave 2019</h1>

                             <p>
                                    Con un diseño espectacular, tres filas de cómodos asientos (2-2-3) y flexibilidad de carga para llevar todo lo que necesitas, Buick Enclave 2019 es donde el tiempo familiar, se convierte en tiempo de calidad.
                                </p>

                            <img alt="Image" src="img/marcas/gmc/enclaveinterior.jpg" />
 
                        </div>

                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>



            <section class="switchable">
                <div class="container">
                    <div class="row justify-content-around">

                        <div class="col-md-12 text-center">

                            <p>
                                Pantalla táctil de 8”, sistema OnStar®† 4G LTE y Full Mirror Display convierten a Buick Enclave en un vehículo moderno y conectado. Escucha tu música favorita, realiza llamadas con manos libres, elige la mejor ruta con los mapas integrados, todo mientras mantienes los ojos en el camino.
                            </p>

                            <img class="img-responsive" src="img/marcas/gmc/enclavepantalla.jpg" alt="Sin imagen" />

                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>





            <section class="text-center bg--secondary">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-10 col-lg-12">
                            <h2>Todo lo que necesitas esta aqui.</h2>
                            <p class="lead">
                                Disfruta de un manejo suave al volante con Buick Enclave Avenir, equipada con AWD y Electronic Twin Clutch, que se activa automáticamente cuando las condiciones del camino lo exigen.*
                            </p>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>
            <section class="bg--secondary">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="feature feature-2 boxed boxed--border">
                                <i class="icon icon-Clock-Back color--primary"></i>
                                <div class="feature__body">
                                    <p>
                                        Prueba de Manejo
                                    </p>
                                </div>
                            </div>
                            <!--end feature-->
                        </div>
                        <div class="col-md-4">
                            <div class="feature feature-2 boxed boxed--border">
                                <i class="icon icon-Duplicate-Window color--primary"></i>
                                <div class="feature__body">
                                    <p>
                                        Cotizacion
                                    </p>
                                </div>
                            </div>
                            <!--end feature-->
                        </div>
                        <div class="col-md-4">
                            <div class="feature feature-2 boxed boxed--border">
                                <i class="icon icon-Life-Jacket color--primary"></i>
                                <div class="feature__body">
                                    <p>
                                        Servicio
                                    </p>
                                </div>
                            </div>
                            <!--end feature-->
                        </div>
                        
                       
                         
                        </div>
                    </div>
                    <!--end of row-->
                </div>  
                <!--end of container-->
            </section>
            <section class="switchable switchable--switch feature-large bg--primary">
                <div class="container">
                    <div class="row justify-content-around">

                        <div class="col-md-3 col-lg-3">
                            
                        </div>

                        <div class="col-md-6 col-12">
                            <div class="video-cover border--round box-shadow-wide">
                                <div class="background-image-holder">
                                    <img alt="image" src="img/landing-8.jpg" />
                                </div>
                                <div class="video-play-icon"></div>
                                <iframe data-src="https://www.youtube.com/embed/fc57JMwUOUI?autoplay=1" allowfullscreen="allowfullscreen"></iframe>
                            </div>
                            <!--end video cover-->
                        </div>
                        <div class="col-md-3 col-lg-3">
                            
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>
            <section class="text-center">
                <div class="container">
                    <div class="row justify-content-center">
                       <div class="col-md-10 col-lg-8">
                            <h2>Obtener mas información.</h2>
                            
                            <form class="row justify-content-center" action="//mrare.us8.list-manage.com/subscribe/post?u=77142ece814d3cff52058a51f&amp;id=f300c9cce8" data-success="Thanks for signing up.  Please check your inbox for a confirmation email." data-error="Please provide your name and email address and agree to the terms.">
                                <div class="col-md-4">
                                    <input class="validate-required" type="text" name="NAME" placeholder="Tu Nombre" />
                                </div>
                                <div class="col-md-4">
                                    <input class="validate-required validate-email" type="email" name="EMAIL" placeholder="Correo Electrónico" />
                                </div>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn--primary type--uppercase">Obtener Información</button>
                                </div>
                                <div class="col-md-12">
                                    <input class="validate-required" type="checkbox" name="group[13737][1]" />
                                    <span>Acepto
                                        <a href="#">terminos y condiciones</a>
                                    </span>
                                </div>
                                <div style="position: absolute; left: -5000px;" aria-hidden="true">
                                    <input type="text" name="b_77142ece814d3cff52058a51f_f300c9cce8" tabindex="-1" value="">
                                </div>
                            </form>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>
            <section class="text-center unpad--bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <img alt="Image" src="img/ebook-2.png" />
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>




           
 </div>
           
             
           
            <div class="modal-container">
                <div class="modal-content">
                    <section class="imageblock feature-large bg--white border--round ">
                        <div class="imageblock__content col-lg-5 col-md-3 pos-left">
                            <div class="background-image-holder">
                                <img alt="image" src="img/cowork-8.jpg" />
                            </div>
                        </div>
                        <div class="container">
                            <div class="row justify-content-end">
                                <div class="col-lg-6 col-md-7">
                                    <div class="row">
                                        <div class="col-md-11 col-lg-10">
                                            <h1>Ideal for design conscious startups.</h1>
                                            <p class="lead">
                                                Start building a beautiful site for your startup &mdash; right in the comfort of your browser.
                                            </p>
                                            <hr class="short">
                                            <form>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <input type="email" name="Email Address" placeholder="Email Address" />
                                                    </div>
                                                    <div class="col-12">
                                                        <input type="password" name="Password" placeholder="Password" />
                                                    </div>
                                                    <div class="col-12">
                                                        <button type="submit" class="btn btn--primary type--uppercase">Create Account</button>
                                                    </div>
                                                    <div class="col-12">
                                                        <span class="type--fine-print">By signing up, you agree to the
                                                            <a href="#">Terms of Service</a>
                                                        </span>
                                                    </div>
                                                </div>
                                                <!--end row-->
                                            </form>
                                        </div>
                                        <!--end of col-->
                                    </div>
                                    <!--end of row-->
                                </div>
                            </div>
                            <!--end of row-->
                        </div>
                        <!--end of container-->
                    </section>
                </div>
            </div>
            <footer class="footer-3 text-center-xs space--xs bg--dark ">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <img alt="Image" class="logo" src="img/logo-dark.png" />

                             
                            <ul class="list-inline list--hover">
                                <li class="list-inline-item">
                                    <a href="#">
                                        <span class="type--fine-print"><i class="fa fa-phone"> </i> 01 800 670 8386 </span>
                                    </a>
                                </li>
                                 
                            </ul>
                        </div>

                         <div class="col-md-5">
                            <span class="type--fine-print">&reg;
                                <span class="update-year"></span> FAME Manantiales.</span>
                            <a class="type--fine-print" href="#">Aviso de Privasidad </a>
                            <a class="type--fine-print" href="#">Formato ARCO</a>
                        </div>

                        <div class="col-md-3 text-right text-center-xs">
                            <ul class="social-list list-inline list--hover">
                                <li class="list-inline-item">
                                    <a href="#">
                                        <i class="socicon socicon-google icon icon--xs"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#">
                                        <i class="socicon socicon-twitter icon icon--xs"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#">
                                        <i class="socicon socicon-facebook icon icon--xs"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#">
                                        <i class="socicon socicon-instagram icon icon--xs"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <!--end of row-->
                    
                    
                </div>
                <!--end of container-->
            </footer>
        </div>
        <!--<div class="loader"></div>-->
        <a class="back-to-top inner-link" href="#start" data-scroll-class="100vh:active">
            <i class="stack-interface stack-up-open-big"></i>
        </a>
        <script src="js/jquery-3.1.1.min.js"></script>
        <script src="js/flickity.min.js"></script>
        <script src="js/easypiechart.min.js"></script>
        <script src="js/parallax.js"></script>
        <script src="js/typed.min.js"></script>
        <script src="js/datepicker.js"></script>
        <script src="js/isotope.min.js"></script>
        <script src="js/ytplayer.min.js"></script>
        <script src="js/lightbox.min.js"></script>
        <script src="js/granim.min.js"></script>
        <script src="js/jquery.steps.min.js"></script>
        <script src="js/countdown.min.js"></script>
        <script src="js/twitterfetcher.min.js"></script>
        <script src="js/spectragram.min.js"></script>
        <script src="js/smooth-scroll.min.js"></script>
        <script src="js/scripts.js"></script>
    </body>
</html>