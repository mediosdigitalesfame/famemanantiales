<div class="nav-container ">
            <div class="bar bar--sm visible-xs">
                <div class="container">
                    <div class="row">
                        <div class="col-3 col-md-2">
                            <a href="index.html">
                                <img class="logo logo-dark" alt="logo" src="img/logo-dark.png" />
                                <img class="logo logo-light" alt="logo" src="img/logo-light.png" />
                            </a>

                        </div>
                        <div class="col-9 col-md-10 text-right">
                            <a href="#" class="hamburger-toggle" data-toggle-class="#menu1;hidden-xs">
                                <i class="icon icon--sm stack-interface stack-menu"></i>
                            </a>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </div>
            <!--end bar-->
            <nav id="menu1" class="bar bar--sm bar-1 hidden-xs " data-scroll-class='366px:pos-fixed'>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-1 col-md-2 hidden-xs">
                            <div class="bar__module">
                                <a href="index.html">
                                    <img class="logo logo-dark" alt="logo" src="img/logo-dark.png" />
                                    <img class="logo logo-light" alt="logo" src="img/logo-light.png" />
                                </a>
                            </div>
                            <!--end module-->
                        </div>
                        <div class="col-lg-11 col-md-12 text-right text-left-xs text-left-sm">
                            <div class="bar__module">
                                <ul class="menu-horizontal text-left">
                                    <li class="dropdown"><a class="dropdown__trigger" href="index.php">
                                        <span class="dropdown__trigger">INICIO</span> </a>
                                         
                                        <!--end dropdown container-->
                                    </li>
                                    <li class="dropdown">
                                        <span class="dropdown__trigger"  href="index.php" >VEHICULOS</span>
                                        <div class="dropdown__container">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="dropdown__content col-lg-2 col-md-4">
                                                        <ul class="menu-vertical">

                                                            <li class="dropdown">
                                                                <span class="dropdown__trigger">BUICK</span>
                                                                <div class="dropdown__container">
                                                                    <div class="container">
                                                                        <div class="row">
                                                                            <div class="dropdown__content col-lg-2">
                                                                                <ul class="menu-vertical">
                                                                                    <li>
                                                                                        <a href="enclave2019.php">
                                                                                            Enclave 2019
                                                                                        </a>
                                                                                    </li>
                                                                                    
                                                                                    <li>
                                                                                        <a href="#">
                                                                                            Modelo 2
                                                                                        </a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                            <!--end dropdown content-->
                                                                        </div>
                                                                        <!--end row-->
                                                                    </div>
                                                                </div>
                                                                <!--end dropdown container-->
                                                            </li>
                                                            
                                                             
                                                            <li class="dropdown separate">
                                                                <span class="dropdown__trigger">GMC</span>
                                                                <div class="dropdown__container">
                                                                    <div class="container">
                                                                        <div class="row">
                                                                            <div class="dropdown__content col-lg-2 col-md-4">
                                                                                <ul class="menu-vertical">
                                                                                    <li>
                                                                                        <a href="#">
                                                                                            Modelo 1
                                                                                        </a>
                                                                                    </li>
                                                                                    
                                                                                    <li>
                                                                                        <a href="#">
                                                                                            Modelo 2
                                                                                        </a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                            <!--end dropdown content-->
                                                                        </div>
                                                                        <!--end row-->
                                                                    </div>
                                                                </div>
                                                                <!--end dropdown container-->
                                                            </li>

                                                            <li class="dropdown separate">
                                                                <span class="dropdown__trigger">CADILLAC</span>
                                                                <div class="dropdown__container">
                                                                    <div class="container">
                                                                        <div class="row">
                                                                            <div class="dropdown__content col-lg-2 col-md-4">
                                                                                <ul class="menu-vertical">
                                                                                    <li>
                                                                                        <a href="#">
                                                                                            Modelo 1
                                                                                        </a>
                                                                                    </li>
                                                                                    
                                                                                    <li>
                                                                                        <a href="#">
                                                                                            Modelo 2
                                                                                        </a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                            <!--end dropdown content-->
                                                                        </div>
                                                                        <!--end row-->
                                                                    </div>
                                                                </div>
                                                                <!--end dropdown container-->
                                                            </li>
                                                            
                                                        </ul>
                                                    </div>
                                                    <!--end dropdown content-->
                                                </div>
                                                <!--end row-->
                                            </div>
                                        </div>
                                        <!--end dropdown container-->
                                    </li>
                                    
                                    <li class="dropdown">
                                        <span class="dropdown__trigger">COMPRAR</span>
                                        <div class="dropdown__container">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="dropdown__content col-lg-2 col-md-4">
                                                         
                                                        <div class="row justify-content-end">
                                                            <div class="col-lg-12">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <ul class="menu-vertical">
                                                                            <li>
                                                                                <a href="#">
                                                                                    Promociones
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#">
                                                                                    Cotizador
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#">
                                                                                    Financiamiento
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#">
                                                                                    Prueba de Manejo
                                                                                </a>
                                                                            </li>
                                                                           
                                                                        </ul>
                                                                    </div>
                                                                     
                                                                </div>
                                                                <!--end of row-->
                                                            </div>
                                                            <!--end of col-->
                                                        </div>
                                                        <!--end of row-->
                                                    </div>
                                                    <!--end dropdown content-->
                                                </div>
                                                <!--end row-->
                                            </div>
                                            <!--end container-->
                                        </div>
                                        <!--end dropdown container-->
                                    </li>

                                    <li class="dropdown">
                                        <span class="dropdown__trigger">SEMINUEVOS</span>
                                        <div class="dropdown__container">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="dropdown__content col-lg-2 col-md-4">
                                                         
                                                        <div class="row justify-content-end">
                                                            <div class="col-lg-12">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <ul class="menu-vertical">
                                                                            <li>
                                                                                <a href="#">
                                                                                    Catalogo
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#">
                                                                                    Auto a Cuenta
                                                                                </a>
                                                                            </li>
                                                                           
                                                                        </ul>
                                                                    </div>
                                                                     
                                                                </div>
                                                                <!--end of row-->
                                                            </div>
                                                            <!--end of col-->
                                                        </div>
                                                        <!--end of row-->
                                                    </div>
                                                    <!--end dropdown content-->
                                                </div>
                                                <!--end row-->
                                            </div>
                                            <!--end container-->
                                        </div>
                                        <!--end dropdown container-->
                                    </li>

                                    <li class="dropdown">
                                        <span class="dropdown__trigger">SERVICIO</span>
                                        <div class="dropdown__container">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="dropdown__content col-lg-2 col-md-4">
                                                         
                                                        <div class="row justify-content-end">
                                                            <div class="col-lg-12">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <ul class="menu-vertical">
                                                                            <li>
                                                                                <a href="citaservicio.php">
                                                                                    Agendar Cita
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="refacciones.php">
                                                                                    Cotizar Refaccion
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#">
                                                                                    Servicio Mantenimiento
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#">
                                                                                    Promocion de Servicio
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#">
                                                                                    ON STAR
                                                                                </a>
                                                                            </li>
                                                                           
                                                                        </ul>
                                                                    </div>
                                                                     
                                                                </div>
                                                                <!--end of row-->
                                                            </div>
                                                            <!--end of col-->
                                                        </div>
                                                        <!--end of row-->
                                                    </div>
                                                    <!--end dropdown content-->
                                                </div>
                                                <!--end row-->
                                            </div>
                                            <!--end container-->
                                        </div>
                                        <!--end dropdown container-->
                                    </li>

                                    <li class="dropdown"><a class="dropdown__trigger" href="contacto.php">
                                        <span class="dropdown__trigger">Contacto</span> </a>
                                         
                                        <!--end dropdown container-->
                                    </li>
                                    
                                </ul>
                            </div>
                            <!--end module-->
                            <div class="bar__module">
                                 
                                <a class="btn btn--sm btn--primary type--lowercase" href="#">
                                    <span class="btn__text">
                                        <i class="fab fa-whatsapp fa-10x"></i>  WhatsApp
                                    </span>
                                </a>
                            </div>
                            <!--end module-->
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </nav>
            <!--end bar-->
        </div>