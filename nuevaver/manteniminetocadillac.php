<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>AUTOS</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Site Description Here">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/stack-interface.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/socicon.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/flickity.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/iconsmind.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/jquery.steps.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/theme.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="css/font-frankruhl-firasans.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Fira+Sans:400,400i,500,700%7CFrank+Ruhl+Libre:300,400" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

    </head>
    <body class=" " data-smooth-scroll-offset='64'>
        <a id="start"></a>
        <div class="nav-container ">
            <div class="bar bar--sm visible-xs">
                <div class="container">
                    <div class="row">
                        <div class="col-3 col-md-2">
                            <a href="index.html">
                                <img class="logo logo-dark" alt="logo" src="img/logo-dark.png" />
                                <img class="logo logo-light" alt="logo" src="img/logo-light.png" />
                            </a>
                        </div>
                        <div class="col-9 col-md-10 text-right">
                            <a href="#" class="hamburger-toggle" data-toggle-class="#menu1;hidden-xs">
                                <i class="icon icon--sm stack-interface stack-menu"></i>
                            </a>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </div>
            <!--end bar-->
            <nav id="menu1" class="bar bar--sm bar-1 hidden-xs " data-scroll-class='366px:pos-fixed'>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-1 col-md-2 hidden-xs">
                            <div class="bar__module">
                                <a href="index.html">
                                    <img class="logo logo-dark" alt="logo" src="img/logo-dark.png" />
                                    <img class="logo logo-light" alt="logo" src="img/logo-light.png" />
                                </a>
                            </div>
                            <!--end module-->
                        </div>
                        <div class="col-lg-11 col-md-12 text-right text-left-xs text-left-sm">
                            <div class="bar__module">
                                <ul class="menu-horizontal text-left">
                                    <li class="dropdown">
                                        <span class="dropdown__trigger">INICIO</span>
                                         
                                        <!--end dropdown container-->
                                    </li>
                                    <li class="dropdown">
                                        <span class="dropdown__trigger">VEHICULOS</span>
                                        <div class="dropdown__container">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="dropdown__content col-lg-2 col-md-4">
                                                        <ul class="menu-vertical">

                                                            <li class="dropdown">
                                                                <span class="dropdown__trigger">BUICK</span>
                                                                <div class="dropdown__container">
                                                                    <div class="container">
                                                                        <div class="row">
                                                                            <div class="dropdown__content col-lg-2">
                                                                                <ul class="menu-vertical">
                                                                                    <li>
                                                                                        <a href="#">
                                                                                            Modelo 1
                                                                                        </a>
                                                                                    </li>
                                                                                    
                                                                                    <li>
                                                                                        <a href="#">
                                                                                            Modelo 2
                                                                                        </a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                            <!--end dropdown content-->
                                                                        </div>
                                                                        <!--end row-->
                                                                    </div>
                                                                </div>
                                                                <!--end dropdown container-->
                                                            </li>
                                                            
                                                             
                                                            <li class="dropdown separate">
                                                                <span class="dropdown__trigger">GMC</span>
                                                                <div class="dropdown__container">
                                                                    <div class="container">
                                                                        <div class="row">
                                                                            <div class="dropdown__content col-lg-2 col-md-4">
                                                                                <ul class="menu-vertical">
                                                                                    <li>
                                                                                        <a href="#">
                                                                                            Modelo 1
                                                                                        </a>
                                                                                    </li>
                                                                                    
                                                                                    <li>
                                                                                        <a href="#">
                                                                                            Modelo 2
                                                                                        </a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                            <!--end dropdown content-->
                                                                        </div>
                                                                        <!--end row-->
                                                                    </div>
                                                                </div>
                                                                <!--end dropdown container-->
                                                            </li>

                                                            <li class="dropdown separate">
                                                                <span class="dropdown__trigger">CADILLAC</span>
                                                                <div class="dropdown__container">
                                                                    <div class="container">
                                                                        <div class="row">
                                                                            <div class="dropdown__content col-lg-2 col-md-4">
                                                                                <ul class="menu-vertical">
                                                                                    <li>
                                                                                        <a href="#">
                                                                                            Modelo 1
                                                                                        </a>
                                                                                    </li>
                                                                                    
                                                                                    <li>
                                                                                        <a href="#">
                                                                                            Modelo 2
                                                                                        </a>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                            <!--end dropdown content-->
                                                                        </div>
                                                                        <!--end row-->
                                                                    </div>
                                                                </div>
                                                                <!--end dropdown container-->
                                                            </li>
                                                            
                                                        </ul>
                                                    </div>
                                                    <!--end dropdown content-->
                                                </div>
                                                <!--end row-->
                                            </div>
                                        </div>
                                        <!--end dropdown container-->
                                    </li>
                                    
                                    <li class="dropdown">
                                        <span class="dropdown__trigger">COMPRAR</span>
                                        <div class="dropdown__container">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="dropdown__content col-lg-2 col-md-4">
                                                         
                                                        <div class="row justify-content-end">
                                                            <div class="col-lg-12">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <ul class="menu-vertical">
                                                                            <li>
                                                                                <a href="#">
                                                                                    Promociones
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#">
                                                                                    Cotizador
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#">
                                                                                    Financiamiento
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#">
                                                                                    Prueba de Manejo
                                                                                </a>
                                                                            </li>
                                                                           
                                                                        </ul>
                                                                    </div>
                                                                     
                                                                </div>
                                                                <!--end of row-->
                                                            </div>
                                                            <!--end of col-->
                                                        </div>
                                                        <!--end of row-->
                                                    </div>
                                                    <!--end dropdown content-->
                                                </div>
                                                <!--end row-->
                                            </div>
                                            <!--end container-->
                                        </div>
                                        <!--end dropdown container-->
                                    </li>

                                    <li class="dropdown">
                                        <span class="dropdown__trigger">SEMINUEVOS</span>
                                        <div class="dropdown__container">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="dropdown__content col-lg-2 col-md-4">
                                                         
                                                        <div class="row justify-content-end">
                                                            <div class="col-lg-12">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <ul class="menu-vertical">
                                                                            <li>
                                                                                <a href="#">
                                                                                    Catalogo
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#">
                                                                                    Auto a Cuenta
                                                                                </a>
                                                                            </li>
                                                                           
                                                                        </ul>
                                                                    </div>
                                                                     
                                                                </div>
                                                                <!--end of row-->
                                                            </div>
                                                            <!--end of col-->
                                                        </div>
                                                        <!--end of row-->
                                                    </div>
                                                    <!--end dropdown content-->
                                                </div>
                                                <!--end row-->
                                            </div>
                                            <!--end container-->
                                        </div>
                                        <!--end dropdown container-->
                                    </li>

                                    <li class="dropdown">
                                        <span class="dropdown__trigger">SERVICIO</span>
                                        <div class="dropdown__container">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="dropdown__content col-lg-2 col-md-4">
                                                         
                                                        <div class="row justify-content-end">
                                                            <div class="col-lg-12">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <ul class="menu-vertical">
                                                                            <li>
                                                                                <a href="#">
                                                                                    Agendar Cita
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#">
                                                                                    Cotizar Refaccion
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#">
                                                                                    Servicio Mantenimiento
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#">
                                                                                    Promocion de Servicio
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#">
                                                                                    ON STAR
                                                                                </a>
                                                                            </li>
                                                                           
                                                                        </ul>
                                                                    </div>
                                                                     
                                                                </div>
                                                                <!--end of row-->
                                                            </div>
                                                            <!--end of col-->
                                                        </div>
                                                        <!--end of row-->
                                                    </div>
                                                    <!--end dropdown content-->
                                                </div>
                                                <!--end row-->
                                            </div>
                                            <!--end container-->
                                        </div>
                                        <!--end dropdown container-->
                                    </li>

                                     <li class="dropdown">
                                        <span class="dropdown__trigger">CONTACTO</span>
                                        
                                        <!--end dropdown container-->
                                    </li>
                                    
                                </ul>
                            </div>
                            <!--end module-->
                            <div class="bar__module">
                                 
                                <a class="btn btn--sm btn--primary type--lowercase" href="#">
                                    <span class="btn__text">
                                        <i class="fab fa-whatsapp fa-10x"></i>  WhatsApp
                                    </span>
                                </a>
                            </div>
                            <!--end module-->
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </nav>
            <!--end bar-->
        </div>
        <div class="main-container">

           

          

             <br>

           
 
           
             
           
            <div class="modal-container">
                <div class="modal-content">
                    <section class="imageblock feature-large bg--white border--round ">
                        <div class="imageblock__content col-lg-5 col-md-3 pos-left">
                            <div class="background-image-holder">
                                <img alt="image" src="img/cowork-8.jpg" />
                            </div>
                        </div>
                        <div class="container">
                            <div class="row justify-content-end">
                                <div class="col-lg-6 col-md-7">
                                    <div class="row">
                                        <div class="col-md-11 col-lg-10">
                                            <h1>Ideal for design conscious startups.</h1>
                                            <p class="lead">
                                                Start building a beautiful site for your startup &mdash; right in the comfort of your browser.
                                            </p>
                                            <hr class="short">
                                            <form>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <input type="email" name="Email Address" placeholder="Email Address" />
                                                    </div>
                                                    <div class="col-12">
                                                        <input type="password" name="Password" placeholder="Password" />
                                                    </div>
                                                    <div class="col-12">
                                                        <button type="submit" class="btn btn--primary type--uppercase">Create Account</button>
                                                    </div>
                                                    <div class="col-12">
                                                        <span class="type--fine-print">By signing up, you agree to the
                                                            <a href="#">Terms of Service</a>
                                                        </span>
                                                    </div>
                                                </div>
                                                <!--end row-->
                                            </form>
                                        </div>
                                        <!--end of col-->
                                    </div>
                                    <!--end of row-->
                                </div>
                            </div>
                            <!--end of row-->
                        </div>
                        <!--end of container-->
                    </section>
                </div>
            </div>
            <footer class="footer-3 text-center-xs space--xs bg--dark ">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <img alt="Image" class="logo" src="img/logo-dark.png" />

                             
                            <ul class="list-inline list--hover">
                                <li class="list-inline-item">
                                    <a href="#">
                                        <span class="type--fine-print"><i class="fa fa-phone"> </i> 01 800 670 8386 </span>
                                    </a>
                                </li>
                                 
                            </ul>
                        </div>

                         <div class="col-md-5">
                            <span class="type--fine-print">&reg;
                                <span class="update-year"></span> FAME Manantiales.</span>
                            <a class="type--fine-print" href="#">Aviso de Privasidad </a>
                            <a class="type--fine-print" href="#">Formato ARCO</a>
                        </div>

                        <div class="col-md-3 text-right text-center-xs">
                            <ul class="social-list list-inline list--hover">
                                <li class="list-inline-item">
                                    <a href="#">
                                        <i class="socicon socicon-google icon icon--xs"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#">
                                        <i class="socicon socicon-twitter icon icon--xs"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#">
                                        <i class="socicon socicon-facebook icon icon--xs"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#">
                                        <i class="socicon socicon-instagram icon icon--xs"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <!--end of row-->
                    
                    
                </div>
                <!--end of container-->
            </footer>
        </div>
        <!--<div class="loader"></div>-->
        <a class="back-to-top inner-link" href="#start" data-scroll-class="100vh:active">
            <i class="stack-interface stack-up-open-big"></i>
        </a>
        <script src="js/jquery-3.1.1.min.js"></script>
        <script src="js/flickity.min.js"></script>
        <script src="js/easypiechart.min.js"></script>
        <script src="js/parallax.js"></script>
        <script src="js/typed.min.js"></script>
        <script src="js/datepicker.js"></script>
        <script src="js/isotope.min.js"></script>
        <script src="js/ytplayer.min.js"></script>
        <script src="js/lightbox.min.js"></script>
        <script src="js/granim.min.js"></script>
        <script src="js/jquery.steps.min.js"></script>
        <script src="js/countdown.min.js"></script>
        <script src="js/twitterfetcher.min.js"></script>
        <script src="js/spectragram.min.js"></script>
        <script src="js/smooth-scroll.min.js"></script>
        <script src="js/scripts.js"></script>
    </body>
</html>