 <?php
	 if (isset($_POST['boton'])) 
         {
			 if($_POST['nombre'] == '') 
			     {
				     $errors[1] = '
				     <div class="alert alert-danger" role="alert" > 
				         <span><i class="fa fa-user"> </i>
				         </span><span class="error">Ingrese su nombre.</span> 
				     </div>';
				 } 
			 else if($_POST['email'] == '' or !preg_match("/^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/",$_POST['email']))
				 {
				     $errors[2] = '
				     <div class="alert alert-danger" role="alert" > 
				         <span><i class="fa fa-envelope"> </i>
				         </span><span class="error">Ingrese un e-Mail correcto.</span> 
				     </div>';
				 } 
			 else if(empty($_POST['telefono'])) 
				 {
				     $errors[3] = '
				     <div class="alert alert-danger" role="alert" > 
				         <span><i class="fa fa-phone"> </i>
				         </span><span class="error">Ingrese un Número Telefonico.</span> 
				     </div>';
				 } 
			 else if($_POST['modelo'] == '0') 
				 {
				     $errors[6] = '
				     <div class="alert alert-danger" role="alert" > 
				         <span><i class="fa fa-pencil"> </i>
				         </span><span class="error">Selecciona un Modelo.</span> 
				     </div>';
				 } 
			 else if(empty($_POST['mensaje'] )) 
				 {
				     $errors[4] = '
				     <div class="alert alert-danger" role="alert" > 
				         <span><i class="fa fa-pencil"> </i>
				         </span><span class="error">Ingrese un mensaje.</span> 
				     </div>';
				 } 
			 else if($_POST['servicio'] == '0') 
				 {
				     $errors[5] = '
				     <div class="alert alert-danger" role="alert" > 
				         <span><i class="fa fa-cog"> </i>
				         </span><span class="error">Selecciona un servicio.</span> 
				     </div>';

				 } 

			 
			 else 
				 {   
				 	 if($_POST['servicio'] == 'Cotización') 
				         {
				         	 $dest = "asesoronline2@contactcenterfame.com" ;
				         }	 
                     else if($_POST['servicio'] == 'Prueba de Manejo GRATIS')
                     	 {
				         	 $dest = "asesoronline2@contactcenterfame.com" ;

				         	   // ventas@famemanantiales.com, gerencia@famemanantiales.com" . ','. "formas@grupofame.com

				         	
                     }
                     else if($_POST['servicio'] == 'Cita de Servicio')
                     	 {
				         	 $dest = "servicio@femamanantiales.com" . ','. "formas@grupofame.com" . ',' . "gerencia@famemanantiales.com" . ',' . "leadspostventafamemanantiales@contactcenterfame.com";
				         }
				     else	
				     	 {
				         	 $dest = "refacciones@famemanantiales.com" . ','. "formas@grupofame.com" . ','. "gerencia@famemanantiales.com" . ',' . "leadspostventafamemanantiales@contactcenterfame.com";
				         }

					 $nombre = $_POST['nombre'];
					 $modelo = $_POST['modelo'];
					 $servicio = $_POST['servicio'];
					 $email = $_POST['email'];
					 $telefono = $_POST['telefono'];
					 $asunto_cte = "Formulario de Contacto FAME Manantiales";
					 $asunto = "Formulario de Contacto FAME Manantiales";
					 $cuerpo = $_POST['mensaje'];
					 $cuerpo_mensaje = '

						 <html>

							 <head>
								 <title>Mail from '. $nombre .'</title>
							 </head>

							 <body>
								 <table style="width: 500px; font-family: arial; font-size: 14px;" border="1">
								     <TH COLSPAN=2 >'. $servicio .'</TH> 
									 <tr style="height: 32px;">
										 <th align="right" style="width:150px; padding-right:5px;">Nombre:</th>
										 <td align="left" style="padding-left:5px; line-height: 20px;">'. $nombre .'</td>
									 </tr>
									 <tr style="height: 32px;">
										 <th align="right" style="width:150px; padding-right:5px;">Correo:</th>
										 <td align="left" style="padding-left:5px; line-height: 20px;">'. $email .'</td>
									 </tr>
									 <tr style="height: 32px;">
										 <th align="right" style="width:150px; padding-right:5px;">Telefono:</th>
										 <td align="left" style="padding-left:5px; line-height: 20px;">'. $telefono .'</td>
									 </tr>
									  <tr style="height: 32px;">
										 <th align="right" style="width:150px; padding-right:5px;">Modelo:</th>
										 <td align="left" style="padding-left:5px; line-height: 20px;">'. $modelo .'</td>
									 </tr>
									 <tr style="height: 32px;">
										 <th align="right" style="width:150px; padding-right:5px;">Mensaje:</th>
										 <td align="left" style="padding-left:5px; line-height: 20px;">'. $cuerpo .'</td>
									 </tr>
								 </table>
							 </body>
						 </html>';
					 $cuerpo_cte = '

						 <html>

							 <head>
								 <title>Mail from '. $nombre .'</title>
							 </head>

							 <body>
								 <table style="width: 500px; font-family: arial; font-size: 14px;" border="1">
								     <tr style="height: 32px;"><TH COLSPAN=2 >CONFIRMAMOS LA RECEPCION DE TUS DATOS</TH> </tr>
<tr style="height: 32px;"><TH COLSPAN=2 >'. $servicio .'</TH> </tr>
									 <tr style="height: 32px;">
										 <th align="right" style="width:150px; padding-right:5px;">Nombre:</th>
										 <td align="left" style="padding-left:5px; line-height: 20px;">'. $nombre .'</td>
									 </tr>
									 <tr style="height: 32px;">
										 <th align="right" style="width:150px; padding-right:5px;">Correo:</th>
										 <td align="left" style="padding-left:5px; line-height: 20px;">'. $email .'</td>
									 </tr>
									 <tr style="height: 32px;">
										 <th align="right" style="width:150px; padding-right:5px;">Telefono:</th>
										 <td align="left" style="padding-left:5px; line-height: 20px;">'. $telefono .'</td>
									 </tr>
									 <tr style="height: 32px;">
										 <th align="right" style="width:150px; padding-right:5px;">Modelo:</th>
										 <td align="left" style="padding-left:5px; line-height: 20px;">'. $modelo .'</td>
									 </tr>
									 <tr style="height: 32px;">
										 <th align="right" style="width:150px; padding-right:5px;">Mensaje:</th>
										 <td align="left" style="padding-left:5px; line-height: 20px;">'. $cuerpo .'</td>
									 </tr>
								 </table>
							 </body>
						 </html>';

					 $headers  = 'MIME-Version: 1.0' . "\r\n";
					 $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
					 $headers .= 'From: ' . $email . "\r\n";

					 $headers2  = 'MIME-Version: 1.0' . "\r\n";
$headers2 .= 'Content-type: text/html; charset=utf-8' . "\r\n";
$headers2 .= "From: GMC Manantiales FAME <contacto@famemanantiales.com>\r\n";
					            
					 if(mail($dest,$asunto,$cuerpo_mensaje,$headers))
					     {
					         $result = '
					             <div class="alert alert-success" role="alert" id="success_message">
         	                         <strong> Mensaje Enviado <i class="fa fa-thumbs-up"></i> </strong> <br> 
         	                         Gracias por Contactarnos, <br> En breve uno de nuestros asesores te atendera.
                                 </div>';
							 mail($email,$asunto_cte,$cuerpo_cte,$headers2);
						     $_POST['nombre'] = '';
						     $_POST['email'] = '';
						     $_POST['telefono'] = '';
						     $_POST['modelo'] = '';
						     $_POST['mensaje'] = '';
						     $_POST['servicio'] = '';
				         } 
				     else 
				         {
				             $result = '
                                 <div class="alert alert-warning" role="alert" id="failure_message">
         	                         Mensaje NO Enviado <i class="fa fa-thumbs-down"></i> <br>
         	                         Intenta Nuevamente.
                                 </div>
				             ';
				         }
				 }
		 }
 ?>

 <div class="container">
     <form class="well2 form-horizontal" action='' method="post"  id="contact_form" action=''>
 
         <fieldset>

         <h2><legend2>¡Nosotros te Contactamos!</legend2></h2>
<labelform>
		 <!-- Input NOMBRE-->
		 <div class="form-group">
		     <div class="col-md-12 inputGroupContainer">
			     <div class="input-group">
			         <span class="input-group-addon2"><i class="fa fa-user"></i></span>
			         <input name="nombre" id="nombre" placeholder="Nombre" class="form-control" type="text" value='<?php if(isset($_POST['nombre'])){ echo $_POST['nombre']; } ?>' >
			     </div>
			 </div>
		 </div>

		 <!-- Input MAIL-->
		 <div class="form-group">
			 <div class="col-md-12 inputGroupContainer">
			     <div class="input-group">
			         <span class="input-group-addon2"><i class="fa fa-envelope"></i></span>
			         <input name="email" id="email" placeholder="Correo Electrónico" class="form-control" type="text" value='<?php if(isset($_POST['email'])){ echo $_POST['email']; } ?>'>
			     </div>
			 </div>
		 </div>

		 <!-- Input TELEFONO-->
		 <div class="form-group"> 
		     <div class="col-md-12 inputGroupContainer">
		         <div class="input-group">
		             <span class="input-group-addon2"><i class="fa fa-phone"></i></span>
		             <input name="telefono" id="telefono" placeholder="Teléfono" class="form-control" type="text" value='<?php if(isset($_POST['telefono'])){ echo $_POST['telefono']; } ?>'>
		         </div>
		     </div>
		 </div>

		 <div class="form-group"> 
		     <div class="col-md-12 selectContainer">
		         <div class="input-group">
			         <span class="input-group-addon2"><i class="fa fa-cog"></i></span>
			         <select name="modelo" name="modelo" id="modelo" class="form-control selectpicker" >
			             <option value='0' >Selecciona un Modelo</option>
			             <option name="m1" id="Beat">Acadia</option>
			             <option name="m2" id="Cavalier">CTS</option>
			             <option name="m3" id="Spark">ATS</option>
			             <option name="m4" id="Beat NB">CTS-V</option>
			             <option name="m4" id="Equinox">ATS-V</option>
			             <option name="m4" id="Aveo">Enclave</option>
			             <option name="m4" id="Cheyenne">Encore</option>
			             <option name="m4" id="Colorado">Envision</option>
			             <option name="m4" id="Cruze">Escalade</option>
			             <option name="m4" id="Suburban">Sierra</option>
			             <option name="m4" id="Tahoe">Sierra All Terrain</option>
			             <option name="m4" id="Tornado">Terrain</option>
			             <option name="m4" id="Trax">XT5</option>
			             <option name="m4" id="Malibu">Yukon</option>

			         </select>
		         </div>
		     </div>
		 </div>

		 <!-- Select SERVICIO -->
		 <div class="form-group"> 
		     <div class="col-md-12 selectContainer">
		         <div class="input-group">
			         <span class="input-group-addon2"><i class="fa fa-cog"></i></span>
			         <select name="servicio" name="servicio" id="servicio" class="form-control selectpicker" >
			         	 <option value='0' >Selecciona una Opción</option>
			             <option name="s1" id="s1">Cotización de Vehiculo</option>
			             <option name="s2" id="s2">Prueba de Manejo GRATIS</option>
			             <option name="s3" id="s3">Cita de Servicio</option>
			             <option name="s4" id="s4">Cotizar Refacciones</option>
			         </select>
		         </div>
		     </div>
		 </div>

		 <!-- Text area --> 
		 <div class="form-group">
		     <div class="col-md-12 inputGroupContainer">
		         <div class="input-group">
		             <span class="input-group-addon2"><i class="fa fa-pencil"></i></span>
		             <textarea maxlength="10000" rows="5" class="form-control" placeholder="Escribe tu Mensaje" id="mensaje" name="mensaje"><?php if(isset($_POST['mensaje'])) { echo $_POST['mensaje']; } ?></textarea>
		         </div>
		     </div>
		 </div>
</labelform>
         <!-- Mensaje ENVIADO -->
         <?php if(isset($result)) { echo $result; } ?>
         

         <!-- Boton ENVIAR -->
         <div class="form-group">
             <label class="col-md-4 control-label"></label>
             <div class="col-md-4">
                 <button name='boton' type="submit" class="btn btn-succes" >ENVIAR <span class="fa fa-paper-plane"></span></button>
             </div>
         </div>

         <div class="form-group">
		     <div class="col-md-12 ">
         	         <?php if(isset($errors)){ echo $errors[1]; } ?>
		     </div>
		     <div class="col-md-12 ">
         	         <?php if(isset($errors)){ echo $errors[2]; } ?>
		     </div>
		     <div class="col-md-12 ">
         	         <?php if(isset($errors)){ echo $errors[3]; } ?>
		     </div>
		     <div class="col-md-12 ">
         	         <?php if(isset($errors)){ echo $errors[4]; } ?>
		     </div>
		     <div class="col-md-12 ">
         	         <?php if(isset($errors)){ echo $errors[5]; } ?>
		     </div>
		     <div class="col-md-12 ">
         	         <?php if(isset($errors)){ echo $errors[6]; } ?>
		     </div>
		 </div>

         </fieldset>
     </form>
 </div>
 