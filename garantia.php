<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>Garantía Extendida</title>
	<?php include('contenido/head.php'); ?>
</head>
<body>

	<?php include('chat.php'); ?>

	<!-- Container -->
	<div id="container">
    	<?php include('contenido/header.php'); ?>
		<?php include('contenido/analytics.php'); ?>

		<div id="content">

			<!-- Page Banner -->
			<div class="page-banner">         

				<div class="container">
					<h2>Garantía </h2>
				</div>
			</div>

			<div class="about-box">
				<div class="container">
					<div class="row">
				</div>
			</div>



<!--barra información limpia-->
		<div class="section">
			<div id="about-section">

				<div class="welcome-box">
					<div class="container">
						<h1><span>GARANTÍA ORDINARIA</span></h1><br>
                        <h2>Servicios básicos Km "0"</h2>
						<p align="justify"><strong>Arrastre de Grúa</strong><br>
A causa de avería, se gestionarán los servicios de remolque desde el lugar del incidente y hasta la agencia más cercana, el usuario deberá acompañar durante el arrastre. Limitado a tres eventos por año.
</p><br><br>

						<p align="justify"><strong>Cambio de llanta</strong><br>
A causa de ponchadura se coordinará un técnico para el cambio exclusivamente por la refacción, en caso de no contar con ésta, se llevará a la vulcanizadora o agencia más cercana. Limitado a tres eventos por año.
</p><br><br>

						<p align="justify"><strong>Suministro de gasolina</strong><br>
Se brindarán 10 litros de gasolina con costo para el Beneficiario. Limitado a tres eventos por año.
</p><br><br>

						<p align="justify"><strong>Paso de corriente</strong><br>
Se enviará a un técnico para pasar corriente a la batería. Limitado a tres eventos por año.
</p><br><br>
         
						<h2>Asistencia hogar</h2>
						<p align="justify"><strong>Cerrajería</strong><br>
Una vez comprobada la propiedad, y si las llaves quedaron dentro del vehículo, se coordinará un cerrajero para abrir la unidad. Si las llaves se extraviaron, se coordinará el servicio de grúa únicamente a la agencia autorizada más cercana. Si es necesario abrir la unidad para liberar la trasmisión y realizar el arrastre, ambos servicios (cerrajero y grúa) serán sin costo para el cliente.<br>
<i>Limitado a tres eventos por año.</i>
</p><br><br>         

						<h2>Asistencia médica</h2>
						<p align="justify"><strong>Medico a domicilio</strong><br>
En caso de lesión o enfermedad del conductor, que se encuentre en territorio nacional, se le enviará un médico y/o se le referirá un centro hospitalario, la totalidad de los gastos generados por este concepto serán por cuenta y riesgo del beneficiario. Con costo preferencial para el beneficiario.
</p><br><br>  

						<p align="justify"><strong>Consultoría medica telefónica</strong><br>
En caso de lesión o enfermedad del conductor, que se encuentre en territorio nacional, se le enviará un médico y/o se le referirá un centro hospitalario, la totalidad de los gastos generados por este concepto serán por cuenta y riesgo del beneficiario.
</p><br><br>  

<!--
<p align="left"><strong>Cobertura</strong></p><br>   
<p align="justify">
La Garantía GM Plus®, le permitirá disfrutar de una cobertura similar a la garantía "Defensa a Defensa" de los primeros 2 años o 60,000 km. de su vehículo General Motors, por un periodo que puede llegar a ser hasta de 3 años más a partir del vencimiento de la garantía original o hasta 60,000 km adicionales (lo que ocurra primero).
</p><br>

<div class="col-md-6">
<p align="justify">
• Motor<br>
• Transmisión<br>
• Transeje (Diferencial) Trasero o Delantero<br>
• Sistema eléctrico<br><br><br>
</p>
</div>

<div class="col-md-6">
<p align="justify">
• Sistema de enfriamiento<br>
• Sistema de frenos<br>
• Dirección<br>
• Suspensión<br>
</p><br><br><br>

</div>
-->
<br><br>						<h1><span>GARANTÍA EXTENDIDA</span></h1><br>
						<p align="justify">
Es un contrato que extiende el tiempo que dura la garantía de tu vehículo cuando concluye la que te ofrece Suzuki Motor de México (tres años o 60,000 Km).
</p><br><br>

						<p align="justify"><strong>Beneficios para nuestros clientes</strong><br>
- Esta garantía protege tu vehículo de forma limitada hasta por seis años o 125,000 Km, lo que ocurra primero.<br>
- Contarás con la asistencia vial las 24 horas durante los 365 días del año en todo el territorio nacional.<br>
- Todas las reparaciones durante la cobertura serán en Concesionarios Autorizados Suzuki.<br>
- Instalación de refacciones originales y mano de obra calificada. Mayor valor de reventa.<br>
- Sin límite de eventos para reparaciones.
</p><br><br>


                        <h2>Parámetros de elegibilidad</h2>
						<p align="justify"><strong>Autos nuevos (Grupo 1):</strong><br>
Se consideran autos nuevos, aquellos vehículos que estén de 0 a10,000 km y de 0 a 6meses, lo que ocurra primero.
</p><br><br>

<p align="justify"><strong>Autos usados (Grupo 2):</strong><br>
Se consideran autos usados, los que estén de 10,001 a 50,000 km y de 6.1 a 30 meses, lo que ocurra primero.
</p><br><br>

<h2>Cobertura Oro</h2><br>
<div class="col-md-6">
<p align="justify">
• Suspensión delantera<br>
• Aire acondicionado<br>
• Motores de vidrios y seguros<br>
• Transmisión<br>
• Tracción delantera<br>
• Tracción trasera<br>
• Dirección<br>
<br>
<br>
</p>
</div>

<div class="col-md-6">
<p align="justify">
• Frenos<br>
• Motores de limpiadores<br>
• Frenos ABS<br>
• Cuadro de instrumentos<br>
• Motor<br>
• Sistema de inyección de combustible<br>
• Sistema eléctrico<br>
</p><br><br><br>
</div>


<h2>Cobertura con asistencia vial</h2><br>
<p align="justify">Además de los sistemas incluidos en la cobertura Oro, este programa incluye:</p><br>

<div class="col-md-6">
<p align="justify">
• Remolque al Concesionario Autorizado SUZUKI más cercano<br>
• Paso de corriente<br>
• Reabastecimiento de gasolina, aceite, etc. (1)<br>
• Cambio de llanta por la de refacción (1)<br>
• Apertura de unidad por olvido de llaves (1)<br>
<br>
<br>
</p>
</div>

<div class="col-md-6">
<p align="justify">
• Transportación a residencia habitual (1)<br>
• Reembolso por gastos de hotel (2)<br>
• Renta de vehículo sustituto (3)<br>
• Regreso o continuación de viaje (3)<br>
• Referencia de Concesionarios Autorizados Suzuki<br>
</p><br><br><br>
</div>

<p align="justify"><i>*1 Aplican restricciones
*2 Hasta tres noches de hotel por evento. Hasta dos eventos por año.
*3 Hasta dos eventos por año.</i></p>

<br><br><br>

<div class="col-md-6">
<h2 align="justify">VEHÍCULOS NUEVOS (GRUPO 1)</h2></div>
<div class="col-md-3">
<p align="justify">
Swift<br>
SX4 / Swift Sport<br>
Grand Vitara / Kizashi<br><br><br>
</p>
</div>

<div class="col-md-3">
<p align="justify">
$ 8,275.00<br>
$ 8,723.00<br>
$ 11,810.00<br><br><br>
<br><br></div>

<br><br>

<div class="col-md-6">
<h2 align="justify">VEHÍCULOS USADOS (GRUPO 2)</h2></div>
<div class="col-md-3">
<p align="justify">
Swift<br>
SX4 / Swift Sport<br>
Grand Vitara / Kizashi<br>
</p>
</div>

<div class="col-md-3">
<p align="justify">
$ 11,187.00<br>
$ 11,635.00<br>
$ 14,721.00<br>
<br><br></div>




<p align="justify">
<br><br>
<strong>*Exclusiones.</strong> Servicios de afinación, lubricantes, líquidos, bolsas de aire y sistemas implicados, gomas de limpiadores, llantas, amortiguadores, alineación, balanceo, discos y pastas del embrague, accesorios, partes estéticas, batería, radio, faros, lámparas y luces, vestiduras, alfombras, molduras, pintura, cromado, desperfectos en la carrocería o soldaduras estructurales, reparaciones para mejorar el desempeño o el funcionamiento causados por el uso y desgaste normal de vehículo, rotura o fallas mecánicas por colisión.<br>

Precios con IVA incluido, sujetos a cambio sin previo aviso. Sólo pagas $550.00 pesos más IVA por evento.
</p>




</div></div>
<?php include('contenido/footer.php'); ?>
     </div> 			
	
</body>
</html>