<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>GM® Financial</title>
	<?php include('contenido/head.php'); ?>
</head>
<body>

	<?php include('chat.php'); ?>

	<!-- Container -->
	<div id="container">
    	<?php include('contenido/header.php'); ?>
		<?php include('contenido/analytics.php'); ?>
		<div id="content">

			<!-- Page Banner -->
			<div class="page-banner">         

				<div class="container">
					<h2>GM® Financial </h2>
				</div>
			</div>

			<div class="about-box">
				<div class="container">
					<div class="row">
				</div>
			</div>



<!--barra información limpia-->
		<div class="section">
			<div id="about-section">

				<div class="welcome-box">
					<div class="container">
						<p align="justify"><strong>GM Financial, la mejor opción para conducir un vehículo BuicK, GMC ó Cadillac.</strong><br><br>
Nuestro objetivo es brindarle los planes de financiamiento y arrendamiento puro más atractivos.</p><br><br><br>

                        <h2>Financiamiento</h2>
<p align="justify"><strong>Plan Tradicional: </strong>Nuestro financiamiento tradicional está enfocado en ofrecerle planes a la medida de sus necesidades para la adquisición de un vehículo.
</p><br><br>

						<p align="justify"><strong>Características</strong><br>
- Enganches desde el 10% del valor del vehículo.<br>
- Plazos desde 3 hasta 60 meses.<br>
- Oportunidad de financiar Accesorios y Garantía Extendida.
</p><br><br>

						<p align="justify"><strong>Beneficios</strong><br>
- El crédito permite deducir hasta $32,500 pesos anualmente, según valor del vehículo y régimen fiscal.<br>
- Pagos mensuales fijos a su medida.<br>
- Opción a pagos irregulares.<br>
- Pagos anticipados sin penalización.
</p><br><br>

						<p align="justify"><strong>Planes Especiales:</strong> Campañas específicas que de tiempo en tiempo se promuevan para líneas y modelos particulares de vehículos según los términos y condiciones establecidas para cada una de ellas.
</p><br><br>

						<p align="justify"><strong>Plan Accesible:</strong> Enfocado a personas que por su actividad económica no tienen posibilidad de comprobar sus ingresos de forma tradicional. GM Financial realiza un estudio socioeconómico para conocer mejor al cliente.
</p><br><br>

						<p align="justify"><strong>Plan Seminuevos:</strong> El cual permite adquirir vehículos con antigüedad de hasta 5 años al modelo actual, con mensualidades atractivas y tasas fijas.
</p><br><br>

						<p align="justify"><strong>Notas:</strong><br>
- Planes sujetos a la aprobación de crédito por parte de GM Financial de México, S.A. de C.V., SOFOM.<br>
- Consulte términos, condiciones, requisitos de contratación y comisiones en <a href="https://www.gmfsecure.com/#inicio" target="_blank"><strong>www.gmfinancial.mx</strong></a><br>
- Los pagos anticipados se sujetan a los términos y condiciones establecidos en el contrato relativo al producto.<br>
- Planes sujetos a cambio sin previo aviso.<br>
- Consulte a su asesor fiscal.
</p><br><br>
         
						<p align="justify">
<i>General Motors de México, S. de R.L. de C.V. y GM Financial de México, S.A. de C.V. (en adelante GM Financial) son personas morales distintas. La contratación, comisiones, términos y condiciones de los planes de arrendamiento financiero y/o créditos a los que se refiere el presente comunicado son responsabilidad exclusiva de GM Financial.</i>
<br><br>
El precio que aparece en este sitio puede diferir del precio ofrecido por el Distribuidor Autorizado BUICK®. El precio del vehículo mostrado en esta página, no representa el precio de compraventa final. Precios sujetos a cambios sin previo aviso (Inflación, paridad con el USD, impactos en el mercado, etc.).<br>
<br>

</p><br><br>         



                        <h2>Arrendamiento puro</h2>
<p align="justify"><strong>Rightlease </strong><br>
Es un producto de arrendamiento puro de unidades, que representa una ventaja para quienes desean conducir un vehículo Buick nuevo con rentas bajas y beneficios fiscales.</p><br><br>

						<p align="justify"><strong>Características</strong><br>
- Opción de incrementar la primera renta para disminuir el monto de las rentas subsecuentes.<br>
- Plazo: Desde 24 hasta 48 meses.<br>
- Los pagos consisten en: <strong>Pago inicial =</strong> Depósito en garantía + primer renta mensual
<strong>Rentas mensuales.</strong> <strong>Valor Residual: </strong>Se paga en el caso de que el cliente desee ejercer su opción de compra del vehículo al terminar el plazo del arrendamiento<br>
- Propiedad: GM Financial es el dueño del vehículo arrendado, por lo que no aparece como activo de la empresa en su balance financiero.<br>
- Seguro del vehículo: El primer año puede ser incluido en el monto de las rentas mensuales o pagarse de contado. El resto de los años será incluido dentro de las rentas mensuales.
</p><br><br>

						<p align="justify"><strong>Beneficios</strong><br>
- Permite deducir hasta $73,000 pesos anuales por unidad y el 100% de los gastos adherentes al uso (seguro del vehículo, mantenimientos, gasolina, etc.). Sólo aplica para unidades nuevas.<br>
- Gran flexibilidad, ya que al final del periodo de renta, el cliente tiene dos opciones: <i>Ejercer la opción de Compra del vehículo ó la devolución del vehículo.</i><br>
</p><br><br>

						<p align="justify"><strong>Notas:</strong><br>
- Arrendamiento sujeto a aprobación de GM Financial de México, S.A. de C.V., SOFOM, E.N.R.<br>
- El pago inicial podría incluir otros conceptos, gastos o comisiones dependiendo de los términos y condiciones de contratación.<br>
- Consulte términos, condiciones, requisitos de contratación y comisiones en Distribuidores autorizados General Motors.<br>
- Consulte a su asesor fiscal.
</p><br><br>

						<p align="justify">
<i>General Motors de México, S. de R.L. de C.V. y GM Financial de México, S.A. de C.V. SOFOMENR (en adelante GM Financial) son personas morales distintas. La contratación, comisiones, términos y condiciones de los planes de arrendamiento puro y/o créditos a los que se refiere el presente comunicado son responsabilidad exclusiva de GM Financial.</i>
<br><br>
El precio que aparece en este sitio puede diferir del precio ofrecido por el Distribuidor Autorizado BUICK®. El precio del vehículo mostrado en esta página, no representa el precio de compraventa final. Precios sujetos a cambios sin previo aviso (Inflación, paridad con el USD, impactos en el mercado, etc.).<br>
<br>

</p><br><br> 

<!--
<h2>Cobertura Oro</h2><br>
<div class="col-md-6">
<p align="justify">
• Suspensión delantera<br>
• Aire acondicionado<br>
• Motores de vidrios y seguros<br>
• Transmisión<br>
• Tracción delantera<br>
• Tracción trasera<br>
• Dirección<br>
<br>
<br>
</p>
</div>

<div class="col-md-6">
<p align="justify">
• Frenos<br>
• Motores de limpiadores<br>
• Frenos ABS<br>
• Cuadro de instrumentos<br>
• Motor<br>
• Sistema de inyección de combustible<br>
• Sistema eléctrico<br>
</p><br><br><br>
</div>

-->


</div></div>
<!--
						<h1><span>Precios</span></h1><br>
							<div class="single-project-content">
								<img alt="" src="images/garantia.jpg">
                         	</div>
                	</div>
				</div>-->
                
                
 <?php include('contenido/footer.php'); ?>
     </div> 			
	
</body>
</html>